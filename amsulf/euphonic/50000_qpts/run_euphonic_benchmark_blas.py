
def main():
    import sys
    if len(sys.argv) > 2:
        nthreads = int(sys.argv[2])
    else:
        nthreads = 0
    if len(sys.argv) > 1:
        nprocs = int(sys.argv[1])
    else:
        nprocs = int(1)

    import ctypes
    blas = ctypes.cdll.LoadLibrary('libopenblas.so')
    if nthreads > 0:
        blas.openblas_set_num_threads(nthreads)

    from euphonic.data.interpolation import InterpolationData
    import numpy as np
    seedname = 'AmSulf_298K'

#    qpts = np.tile(np.loadtxt('10meV_qpts.txt'), (5,1))
    qpts = np.load('../../50000_qpts.npy')
    idata = InterpolationData(seedname)
    N = 1
    print(('Calculating phonons for {:s}\nN qpts: {:d}\nN repeats: {:d}\nN '
           'cpus: {:d}\n N threads: {:d}').format(seedname, len(qpts), N,
                                                  nprocs, nthreads))
    for i in range(N):
        idata.calculate_fine_phonons(qpts, asr='reciprocal', nprocs=nprocs, eta_scale=0.4)
    print('Freqs for qpt 5000:')
    print(idata.freqs[5000])

if __name__ == '__main__':
    main()
