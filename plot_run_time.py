import os
import re
import numpy as np
import matplotlib.pyplot as plt

def dirname(seedname, folder_suffix='_nt1'):
    return seedname + '/castep/' + seedname + folder_suffix

def slurm_fnames(seedname, nprocs, **kwargs):
    dname = dirname(seedname, **kwargs)
    files = os.listdir(dname)
    regex = re.compile('slurm-\d+_' + str(nprocs) + '.out')
    matches = []
    for fname in files:
        if regex.match(fname):
            matches.append(dname + '/' + fname)
    return matches

def cprofile_fname(seedname, nprocs, **kwargs):
    dname = dirname(seedname, **kwargs)
    fname = dname + '/' + seedname + '_np' + str(nprocs) + '.cprofile'
    return fname

def get_max_rss(seedname, nprocs, **kwargs):
    regex = re.compile(('Maximum resident set size \(kbytes\): (\d+)'))
    return get_values(seedname, nprocs, regex, time=False, **kwargs)

def get_minor_faults(seedname, nprocs, **kwargs):
    regex = re.compile(('Minor \(reclaiming a frame\) page faults: (\d+)'))
    return get_values(seedname, nprocs, regex, time=False, **kwargs)

def get_walltimes(seedname, nprocs, **kwargs):
    regex = re.compile(('Elapsed \(wall clock\) time \(h:mm:ss or m:ss\): '
                        '(\S+)'))
    return get_values(seedname, nprocs, regex, time=True, **kwargs)

def get_cprofile_time(seedname, nprocs, funcstr='calculate_dyn_mats', **kwargs):
    fname = cprofile_fname(seedname, nprocs, **kwargs)
    regex = re.compile(('\d+\s+(\d+\.\d+)\s+\d+\.\d+\s+\d+\.\d+\s+\d+\.\d+.*' + funcstr))
    with open(fname, 'r') as f:
        data = f.readlines()
        for line in data:
            match = regex.search(line)
            if match:
                ctime = float(match.group(1))
                break
    return ctime

def get_values(seedname, nprocs, regex, time=False, **kwargs):
    fnames = slurm_fnames(seedname, nprocs, **kwargs)
    values = []
    for i, fname in enumerate(fnames):
        with open(fname, 'r') as f:
            data = f.readlines()
        for line in data:
            match = regex.search(line)
            if match:
                if time:
                    timestr = match.group(1).split(':')
                    val = int(timestr[-2])*60 + float(timestr[-1])
                    if len(timestr) > 2:
                        val += int(timestr[-3])*3600
                else:
                    val = int(match.group(1))
                values.append(val)
                break
    return values

def get_linear_scaling(walltimes, procs):
    timings = np.zeros(walltimes.shape)
    timings[:, 0] = walltimes[:, 0]
    for i, proc in enumerate(procs[1:]):
         timings[:, i + 1] = walltimes[:, 0]/proc
    return timings

#seednames = ['nb', 'lzo', 'quartz', 'amsulf']
#seednames = ['nb', 'lzo', 'quartz']
seednames = ['lzo']
procs = [1, 2, 4, 8, 12, 24]

# Get walltimes
walltimes = np.zeros((len(seednames), len(procs)))
for i, sname in enumerate(seednames):
    print(sname)
    for j, proc in enumerate(procs):
        print(proc)
        walltimes[i, j] = np.mean(get_walltimes(sname, proc, folder_suffix='_norecip'))
perfect_scaling = get_linear_scaling(walltimes, procs)
speedup = walltimes[:, 0, np.newaxis]/walltimes

# Get minor faults
minfaults = np.zeros((len(seednames), len(procs)))
for i, sname in enumerate(seednames):
    for j, proc in enumerate(procs):
        minfaults[i, j] = np.mean(get_minor_faults(sname, proc))

# Get max RSS
maxrss = np.zeros((len(seednames), len(procs)))
for i, sname in enumerate(seednames):
    for j, proc in enumerate(procs):
        maxrss[i, j] = np.mean(get_max_rss(sname, proc))

# Get other walltimes
#procs = [1, 2, 4, 8, 12, 24, 48]
suffix = '_norecip_c'
walltimes2 = np.zeros((len(seednames), len(procs)))
cprofiletimes2 = np.zeros((len(seednames), len(procs)))
for i, sname in enumerate(seednames):
    for j, proc in enumerate(procs):
        walltimes2[i, j] = np.mean(get_walltimes(
            sname, proc, folder_suffix=suffix))
        cprofiletimes2[i, j] = get_cprofile_time(sname, proc, folder_suffix=suffix)
perfect_scaling2 = get_linear_scaling(walltimes2, procs)
speedup2 = walltimes2[:, 0, np.newaxis]/walltimes2
cprofilespeedup2 = cprofiletimes2[:, 0, np.newaxis]/cprofiletimes2
#minfaults2 = np.zeros((len(seednames), len(procs)))
#for i, sname in enumerate(seednames):
#    for j, proc in enumerate(procs):
#        minfaults2[i, j] = np.mean(get_minor_faults(sname, proc,
#                                   folder_suffix=suffix))

# Plot
colors = ['r', 'g', 'b', 'c']
fig1, ax1 = plt.subplots(1,1)
for i in range(len(walltimes)):
    ax1.plot(procs, walltimes[i], label=seednames[i] + ' multiprocessing', color=colors[i+1])
#    ax1.plot(procs, perfect_scaling[i], color=colors[i], linestyle='--')
    try:
        ax1.plot(procs, walltimes2[i], label=seednames[i] + ' C OpenMP',
                 color=colors[i], linestyle='-')
#        ax1.plot(procs, perfect_scaling2[i], color=colors[i], linestyle=':')
    except NameError:
        pass
ax1.legend()
ax1.set_ylim(0)
ax1.set_xlim(1)
ax1.set_xlabel('Number of processes')
ax1.set_ylabel('Walltime (s)')
fig1.suptitle('Walltime for eigenvalue/vector calculation at 50,000 q-points')
fig1.show()

# Log plot
fig2, ax2 = plt.subplots(1,1)
ax2.set_yscale('log')
for i in range(len(walltimes)):
    ax2.plot(procs, walltimes[i], label=seednames[i] + ' multiprocessing', color=colors[i+1])
#    ax2.plot(procs, perfect_scaling[i], color=colors[i], linestyle='--')
    try:
        ax2.plot(procs, walltimes2[i], label=seednames[i] + ' C OpenMP',
                 color=colors[i], linestyle='-')
#        ax2.plot(procs, perfect_scaling2[i], color=colors[i], linestyle=':')
    except NameError:
        pass
ax2.legend()
ax2.set_xlim(1)
ax2.set_xlabel('Number of processes')
ax2.set_ylabel('Walltime (s)')
fig2.suptitle('Total walltime for eigenvalue/vector calculation at 50,000 q-points')
fig2.show()

# Speedup plot
fig3, ax3 = plt.subplots(1,1)
for i in range(len(walltimes)):
    ax3.plot(procs, speedup[i], label=seednames[i] + ' multiprocessing', color=colors[i+1])
    try:
        ax3.plot(procs, speedup2[i], label=seednames[i] + '  C OpenMP',
                 color=colors[i], linestyle='-')
    except NameError:
        pass
ax3.plot(procs, procs, color='k')
ax3.legend()
ax3.set_ylim(1)
ax3.set_xlim(1)
ax3.set_xlabel('Number of processes')
ax3.set_ylabel('Speedup')
fig3.suptitle('Total walltime speedup for eigenvalue/vector calculation at 50,000 q-points')
fig3.show()

## Minor page faults plot
#fig4, ax4 = plt.subplots(1,1)
#for i in range(len(minfaults)):
#    ax4.plot(procs, minfaults[i], label=seednames[i], color=colors[i])
#    try:
#        ax4.plot(procs, minfaults2[i], label=seednames[i] + ' shmem',
#                 color=colors[i], linestyle='--')
#    except NameError:
#        pass
#ax4.legend()
#ax4.set_xlim(1)
#ax4.set_xlabel('Number of processes')
#ax4.set_ylabel('Number of faults')
#fig4.suptitle('Minor page faults for eigenvalue/vector calculation at 50,000 q-points')
#fig4.show()

# Max RSS plot
#fig5, ax5 = plt.subplots(1,1)
#for i in range(len(minfaults)):
#    ax5.plot(procs, maxrss[i], label=seednames[i], color=colors[i])
#    try:
#        ax5.plot(procs, maxrss2[i], label=seednames[i] + ' shmem',
#                 color=colors[i], linestyle='--')
#    except NameError:
#        pass
#ax5.legend()
#ax5.set_xlim(1)
#ax5.set_xlabel('Number of processes')
#ax5.set_ylabel('Max RSS (kb)')
#fig5.suptitle('Max Resident Set Size for eigenvalue/vector calculation at 50,000 q-points')
#fig5.show()

# CProfile plots
colors = ['r', 'g', 'b', 'c']
fig6, ax6 = plt.subplots(1,1)
for i in range(len(cprofiletimes2)):
    ax6.plot(procs, cprofiletimes2[i], label=seednames[i] + ' C OpenMP', color=colors[i])
ax6.legend()
ax6.set_ylim(0)
ax6.set_xlim(1)
ax6.set_xlabel('Number of processes')
ax6.set_ylabel('Time (s)')
fig6.suptitle('CProfile time for the C eigenvalue/vector calculation only for 50,000 q-points')
fig6.show()

# Log plot
fig7, ax7 = plt.subplots(1,1)
ax7.set_yscale('log')
for i in range(len(cprofiletimes2)):
    ax7.plot(procs, cprofiletimes2[i], label=seednames[i] + ' C OpenMP', color=colors[i])
ax7.legend()
ax7.set_xlim(1)
ax7.set_xlabel('Number of processes')
ax7.set_ylabel('Time (s)')
fig7.suptitle('CProfile time for the C eigenvalue/vector calculation only for 50,000 q-points')
fig7.show()

# Speedup plot
fig8, ax8 = plt.subplots(1,1)
for i in range(len(cprofilespeedup2)):
    ax8.plot(procs, cprofilespeedup2[i], label=seednames[i] + ' C OpenMP', color=colors[i])
ax8.plot(procs, procs, color='k')
ax8.legend()
ax8.set_ylim(1)
ax8.set_xlim(1)
ax8.set_xlabel('Number of processes')
ax8.set_ylabel('Speedup')
fig8.suptitle('Speedup for the C eigenvalue/vector calculation only for 50,000 q-points')
fig8.show()

