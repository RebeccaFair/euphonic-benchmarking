import numpy as np
APPLYTIMEREVERSAL=0
Temperature=100
resolutionfile='resolution.txt'
Saturation=0.3
lowerLimit=0
bottom_meV=0
NEUTRONCALC=1
CohB={'Si': 4.1491, 'O': 5.803}
NeutronE=100
branchWeight=np.ones(27).tolist()
EigScal=0
UniqueIon=-1

# 0510 (-0.5, 1., 0) cut
redStarts = [[-0.025000, -7.950000, -1.500000]]
redEnds = [[-7.625000,  7.250000, -1.500000]]
Nqlines = [305]
