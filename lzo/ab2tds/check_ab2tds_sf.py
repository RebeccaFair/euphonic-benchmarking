import numpy as np
import matplotlib.pyplot as plt
import sys
import os
from simphony.data.phonon import PhononData
from simphony.data.interpolation import InterpolationData
from simphony.calculate.scattering import structure_factor, sqw_map
from simphony.plot.dispersion import plot_sqw_map

def sum_degenerate_modes(freqs, sf):
    # Sum structure factors for degenerate modes
    unique, unique_index, unique_modes = np.unique(freqs, return_index=True, return_inverse=True)
    # Determine which degenerate modes are acoustic
    ac = (unique_index <= 2)
    n = len(unique)
    mode_sum = np.bincount(unique_modes, sf)
    return n, mode_sum, ac

def get_scale_factor(in1, in2, sigma=1.0):
    # Avoid /0 in sf_pdata
    lim = 10e-5
    ratio = in2[in1 > lim]/in1[in1 > lim]
    scale = np.mean(ratio[abs(ratio - np.mean(ratio)) < sigma*np.std(ratio)])
    return scale

def plot_sf(data, labels, qpt_num, qpt, title=''):
    fig, subplot = plt.subplots(1, 1)
    for i in range(len(data)):
        try:
            subplot.plot(data[i][qpt_num], label=labels[i])
        except NameError:
            pass
    #subplot.title('Summed structure factors at ' + str(qpt))
    if title:
        fig.suptitle(title)
    else:
        fig.suptitle('Summed structure factors at ' + str(qpt))
    subplot.legend()
    return fig

def get_ab2tds_data(data_file):
    data = np.loadtxt(data_file)
    qpts = data[:, :3]
    freqs = data[:, range(3, data.shape[1], 3)]
    intensities = data[:, range(4, data.shape[1], 3)]
    anti_intensities = data[:, range(5, data.shape[1], 3)]
    return qpts, freqs, intensities, anti_intensities
 
# Parameters
curr_dir = os.getcwd()
if len(sys.argv) > 1:
    cut_dir = sys.argv[1]
else:
    cut_dir = '0-1-1_scan/'
abs_cut_dir = curr_dir + '/' + cut_dir
sys.path.append(abs_cut_dir)
interpolation_dir = abs_cut_dir + 'outputs/interpolation/'
interpolation_no_dw_dir = abs_cut_dir + 'outputs/interpolation_no_dw/'
no_interpolation_dir = abs_cut_dir + 'outputs/no_interpolation/'
no_interpolation_no_dw_dir = abs_cut_dir + 'outputs/no_interpolation_no_dw/'
out_file = 'alongthelineF.dat'
pdata_dir = curr_dir + '/../castep/' + cut_dir
idata_dir = curr_dir + '/../castep'

seedname = 'La2Zr2O7'
import intensity_map_input as imi
sl = imi.CohB
T=imi.Temperature
vmin = imi.lowerLimit
vmax = imi.Saturation

# Read ab2tds data without interpolation
qpts, freqs, intensities, anti_intensities = get_ab2tds_data(no_interpolation_dir + out_file)
res = np.loadtxt(imi.resolutionfile)

# Read ab2tds data without interpolation and without DW
qpts_no_dw, freqs_no_dw, intensities_no_dw, anti_intensities_no_dw = get_ab2tds_data(no_interpolation_no_dw_dir + out_file)
res = np.loadtxt(imi.resolutionfile)

# Read ab2tds data with interpolation
qpts_i, freqs_i, intensities_i, anti_intensities_i = get_ab2tds_data(interpolation_dir + out_file)
res_i = np.loadtxt(imi.resolutionfile)

# Read ab2tds data with interpolation and without DW
qpts_i_no_dw, freqs_i_no_dw, intensities_i_no_dw, anti_intensities_i_no_dw = get_ab2tds_data(interpolation_no_dw_dir + out_file)

# Read latest ab2tds run
try:
    qpts_latest, freqs_latest, intensities_latest, anti_intensities_latest = get_ab2tds_data(out_file)
except EnvironmentError:
    pass

# Get ebins
ebins = np.loadtxt(interpolation_dir + 'ebins.dat')
ebin_edges = np.zeros(len(ebins) + 1)
ebin_width = np.mean(np.diff(ebins))
ebin_edges[:-1] = ebins - ebin_width/2
ebin_edges[-1] = ebins[-1] + ebin_width/2

# Make SimPhony objects
pdata = PhononData(seedname, pdata_dir)
idata = InterpolationData(seedname, idata_dir)
idata.calculate_fine_phonons(pdata.qpts, asr=True)

# Calculate structure factor summed over degenerate modes for SimPhony and ab2tds data
# And calculate scale for each qpt
sf_pdata = structure_factor(pdata, sl, T=T, dw_seedname='La2Zr2O7-6-grid')
sf_idata = structure_factor(idata, sl, T=T, dw_grid=[6,6,6])
#sf_pdata = structure_factor(pdata, sl, T=T)
#sf_idata = structure_factor(idata, sl, T=T)
sf_pdata_sum = np.zeros(sf_pdata.shape)
sf_idata_sum = np.zeros(sf_idata.shape)
intensities_sum = np.zeros(intensities.shape)
intensities_no_dw_sum = np.zeros(intensities_no_dw.shape)
intensities_i_sum = np.zeros(intensities_i.shape)
intensities_i_no_dw_sum = np.zeros(intensities_i_no_dw.shape)
try:
    intensities_latest_sum = np.zeros(intensities_latest.shape)
except NameError:
    pass
# ac shows which summed modes are acoustic
ac = np.zeros(sf_pdata.shape, dtype=bool)

for q in range(pdata.n_qpts):
    n, sf_pdata_sum[q, :n], ac[q, :n] = sum_degenerate_modes(pdata.freqs[q], sf_pdata[q])
    n, sf_idata_sum[q, :n], ac[q, :n] = sum_degenerate_modes(pdata.freqs[q], sf_idata[q])
    n, intensities_sum[q, :n], ac[q, :n] = sum_degenerate_modes(pdata.freqs[q], intensities[q])
    n, intensities_no_dw_sum[q, :n], ac[q, :n] = sum_degenerate_modes(pdata.freqs[q], intensities_no_dw[q])
    n, intensities_i_sum[q, :n], ac[q, :n] = sum_degenerate_modes(pdata.freqs[q], intensities_i[q])
    n, intensities_i_no_dw_sum[q, :n], ac[q, :n] = sum_degenerate_modes(pdata.freqs[q], intensities_i_no_dw[q])
    try:
        n, intensities_latest_sum[q, :n], ac[q, :n] = sum_degenerate_modes(pdata.freqs[q], intensities_latest[q])
    except NameError:
        pass
# Don't scale SimPhony output
#sf_idata_sum = sf_idata_sum*get_scale_factor(sf_idata_sum, sf_pdata_sum)
#intensities_sum = intensities_sum*get_scale_factor(intensities_sum, sf_pdata_sum)
#sf_pdata_sum = sf_pdata_sum*get_scale_factor(sf_pdata_sum, sf_idata_sum)
intensities_sum = intensities_sum*get_scale_factor(intensities_sum, sf_idata_sum)
intensities_no_dw_sum = intensities_no_dw_sum*get_scale_factor(intensities_no_dw_sum, sf_pdata_sum)
intensities_i_sum = intensities_i_sum*get_scale_factor(intensities_i_sum[1:10], sf_pdata_sum[1:10])
intensities_i_no_dw_sum = intensities_i_no_dw_sum*get_scale_factor(intensities_i_no_dw_sum[1:10], sf_pdata_sum[1:10])
try:
    intensities_latest_sum = intensities_latest_sum*get_scale_factor(intensities_latest_sum, sf_pdata_sum)
except NameError:
    pass

try:
    data = [sf_pdata_sum, sf_idata_sum, intensities_sum, intensities_no_dw_sum, intensities_i_sum, intensities_i_no_dw_sum, intensities_latest_sum]
    freqs_arr = [pdata.freqs, idata.freqs, freqs, freqs_no_dw, freqs_i, freqs_i_no_dw, freqs_latest]
except NameError:
    data = [sf_pdata_sum, sf_idata_sum, intensities_sum, intensities_no_dw_sum, intensities_i_sum, intensities_i_no_dw_sum]
    freqs_arr = [pdata.freqs, idata.freqs, freqs, freqs_i, freqs_i_no_dw]
labels = ['pdata', 'idata', 'ab2tds', 'ab2tds no DW', 'ab2tds interpolation', 'ab2tds interpolation no DW', 'ab2tds latest']



# Compare with either idata or pdata
compare = sf_pdata_sum

# Calculate percentage difference
tol = 1e-11
#tol = 1e-8
i = np.where(compare > tol)
j = np.where(compare[~ac] > tol)

# With acoustic modes
simphony_diff = np.abs(sf_idata_sum[i] - compare[i])/compare[i]*100
ab2tds_diff = np.abs(intensities_sum[i] - compare[i])/compare[i]*100
ab2tds_no_dw_diff = np.abs(intensities_no_dw_sum[i] - compare[i])/compare[i]*100
ab2tds_i_diff = np.abs(intensities_i_sum[i] - compare[i])/compare[i]*100
ab2tds_i_no_dw_diff = np.abs(intensities_i_no_dw_sum[i] - compare[i])/compare[i]*100

# Without acoustic modes
simphony_diff_no_ac = np.abs(sf_idata_sum[~ac][j] - compare[~ac][j])/compare[~ac][j]*100
ab2tds_diff_no_ac = np.abs(intensities_sum[~ac][j] - compare[~ac][j])/compare[~ac][j]*100
ab2tds_no_dw_diff_no_ac = np.abs(intensities_no_dw_sum[~ac][j] - compare[~ac][j])/compare[~ac][j]*100
ab2tds_i_diff_no_ac = np.abs(intensities_i_sum[~ac][j] - compare[~ac][j])/compare[~ac][j]*100
ab2tds_i_no_dw_diff_no_ac = np.abs(intensities_i_no_dw_sum[~ac][j] - compare[~ac][j])/compare[~ac][j]*100

# Find gamma equivalent points
g = (np.sum(np.mod(pdata.qpts, 1), axis=1) == 0)
g = np.tile(g, (pdata.n_branches, 1)).transpose()
k = np.where(compare[~g] > tol)
# Without gamma points
simphony_diff_no_g = np.abs(sf_idata_sum[~g][k] - compare[~g][k])/compare[~g][k]*100
ab2tds_diff_no_g = np.abs(intensities_sum[~g][k] - compare[~g][k])/compare[~g][k]*100
ab2tds_no_dw_diff_no_g = np.abs(intensities_no_dw_sum[~g][k] - compare[~g][k])/compare[~g][k]*100
ab2tds_i_diff_no_g = np.abs(intensities_i_sum[~g][k] - compare[~g][k])/compare[~g][k]*100
ab2tds_i_no_dw_diff_no_g = np.abs(intensities_i_no_dw_sum[~g][k] - compare[~g][k])/compare[~g][k]*100

strlen = '30'
print('% Difference from .phonon:')
print('WITH acoustic modes:')
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('SimPhony interpolation', np.mean(simphony_diff), np.amin(simphony_diff), np.amax(simphony_diff)))
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('Ab2tds', np.mean(ab2tds_diff), np.amin(ab2tds_diff), np.amax(ab2tds_diff)))
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('Ab2tds no DW', np.mean(ab2tds_no_dw_diff), np.amin(ab2tds_no_dw_diff), np.amax(ab2tds_no_dw_diff)))
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('Ab2tds interpolation', np.mean(ab2tds_i_diff), np.amin(ab2tds_i_diff), np.amax(ab2tds_i_diff)))
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('Ab2tds interpolation no DW', np.mean(ab2tds_i_no_dw_diff), np.amin(ab2tds_i_no_dw_diff), np.amax(ab2tds_i_no_dw_diff)))
print('WITHOUT acoustic modes:')
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('SimPhony interpolation', np.mean(simphony_diff_no_ac), np.amin(simphony_diff_no_ac), np.amax(simphony_diff_no_ac)))
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('Ab2tds', np.mean(ab2tds_diff_no_ac), np.amin(ab2tds_diff_no_ac), np.amax(ab2tds_diff_no_ac)))
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('Ab2tds no DW', np.mean(ab2tds_no_dw_diff_no_ac), np.amin(ab2tds_no_dw_diff_no_ac), np.amax(ab2tds_no_dw_diff_no_ac)))
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('Ab2tds interpolation', np.mean(ab2tds_i_diff_no_ac), np.amin(ab2tds_i_diff_no_ac), np.amax(ab2tds_i_diff_no_ac)))
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('Ab2tds interpolation no DW', np.mean(ab2tds_i_no_dw_diff_no_ac), np.amin(ab2tds_i_no_dw_diff_no_ac), np.amax(ab2tds_i_no_dw_diff_no_ac)))
print('WITHOUT gamma points:')
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('SimPhony interpolation', np.mean(simphony_diff_no_g), np.amin(simphony_diff_no_g), np.amax(simphony_diff_no_g)))
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('Ab2tds', np.mean(ab2tds_diff_no_g), np.amin(ab2tds_diff_no_g), np.amax(ab2tds_diff_no_g)))
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('Ab2tds no DW', np.mean(ab2tds_no_dw_diff_no_g), np.amin(ab2tds_no_dw_diff_no_g), np.amax(ab2tds_no_dw_diff_no_g)))
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('Ab2tds interpolation', np.mean(ab2tds_i_diff_no_g), np.amin(ab2tds_i_diff_no_g), np.amax(ab2tds_i_diff_no_g)))
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('Ab2tds interpolation no DW', np.mean(ab2tds_i_no_dw_diff_no_g), np.amin(ab2tds_i_no_dw_diff_no_g), np.amax(ab2tds_i_no_dw_diff_no_g)))

qpt = 0
fig = plot_sf(data, labels, qpt, str(idata.qpts[qpt]))
fig.show()

qpt = 7
fig = plot_sf(data, labels, qpt, str(idata.qpts[qpt]))
fig.show()
#fig2 = plot_sf(freqs_arr, labels, qpt, str(idata.qpts[qpt]), title='Freqs at ' + str(pdata.qpts[qpt]))

fig1.show()
#fig2.show()
