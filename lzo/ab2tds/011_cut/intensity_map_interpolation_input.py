import numpy as np
APPLYTIMEREVERSAL=0
Temperature=100
resolutionfile='resolution.txt'
Saturation=0.5
lowerLimit=0
bottom_meV=0
NEUTRONCALC=1
CohB={'La': 8.24, 'Zr': 7.16, 'O': 5.803}
NeutronE=100
branchWeight=np.ones(66).tolist()
EigScal=0
UniqueIon=-1

Nfour_interp=4
tth_max=45
redStarts = [[0.000000, 0.000000, -6.000000]]
redEnds = [[0.000000, 10.000000, 4.000000]]
Nqlines = [500]

