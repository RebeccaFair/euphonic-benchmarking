import numpy as np
import matplotlib.pyplot as plt
import sys
import os
from simphony.data.phonon import PhononData
from simphony.data.interpolation import InterpolationData
from simphony.calculate.scattering import structure_factor, sqw_map
from simphony.plot.dispersion import plot_sqw_map

# Parameters
curr_dir = os.getcwd()
if len(sys.argv) > 1:
    cut_dir = sys.argv[1]
else:
    cut_dir = '0-1-1_scan/'
abs_cut_dir = curr_dir + '/' + cut_dir
sys.path.append(abs_cut_dir)
interpolation_dir = abs_cut_dir + 'outputs/interpolation/'
no_interpolation_dir = abs_cut_dir + 'outputs/no_interpolation/'
out_file = 'res.dat'
pdata_dir = curr_dir + '/../castep/' + cut_dir
idata_dir = curr_dir + '/../castep'

seedname = 'La2Zr2O7'
import intensity_map_input as imi
sl = imi.CohB
T=imi.Temperature
vmin = imi.lowerLimit
vmax = imi.Saturation

# Read ab2tds data without interpolation
data = np.transpose(np.loadtxt(no_interpolation_dir + out_file))
res = np.loadtxt(imi.resolutionfile)

# Read ab2tds data with interpolation
data_i = np.transpose(np.loadtxt(interpolation_dir + out_file))
res_i = np.loadtxt(imi.resolutionfile)

# Get ebins
ebins = np.loadtxt(interpolation_dir + 'ebins.dat')
ebin_edges = np.zeros(len(ebins) + 1)
ebin_width = np.mean(np.diff(ebins))
ebin_edges[:-1] = ebins - ebin_width/2
ebin_edges[-1] = ebins[-1] + ebin_width/2

# Make SimPhony objects
pdata = PhononData(seedname, pdata_dir)
pdata.convert_e_units('meV')
idata = InterpolationData(seedname, idata_dir)
idata.calculate_fine_phonons(pdata.qpts, asr=True)
idata.convert_e_units('meV')

# Calculate sqw_map
sqw_map(idata, ebin_edges, sl, ewidth=1.5, qwidth=0.1)
sqw_map(pdata, ebin_edges, sl, ewidth=1.5, qwidth=0.1)
#sqw_map(idata, ebin_edges, sl)
#sqw_map(pdata, ebin_edges, sl)

lim = 100
data = np.flip(data[:,:100], axis=1)
data_i = np.flip(data_i[:,:100], axis=1)
idata.sqw_map = idata.sqw_map[:, -100:]
pdata.sqw_map = pdata.sqw_map[:, -100:]

# Avoid /0 in pdata.sqw_map and ignore low energy bins
sqw_cut = pdata.sqw_map[:, 5:]
data_cut = data_i[:, 5:]
ratio = sqw_cut[data_cut > 10e-5]/data_cut[data_cut > 10e-5]
sigma = 1
scale = np.mean(ratio[abs(ratio - np.mean(ratio)) < sigma*np.std(ratio)])
data = data*scale
data_i = data_i*scale

#f1 = plt.figure(1)
#plt.imshow(idata.sqw_map[:,:])
#f1.show()

#f2 = plt.figure(2)
#plt.imshow(data[:,:])
#f2.show()

qpt = 0
f3 = plt.figure(3)
plt.plot(idata.sqw_map[qpt, :], label='simphony_idata')
plt.plot(pdata.sqw_map[qpt, :], label='simphony_pdata')
plt.plot(data[qpt, :], label='ab2tds')
plt.plot(data_i[qpt, :], label='ab2tds interpolation')
plt.title('Energy bin intensities at ' + str(idata.qpts[qpt]))
plt.legend()
f3.show()

qpt = 7
f4 = plt.figure(4)
plt.plot(idata.sqw_map[qpt, :], label='simphony_idata')
plt.plot(pdata.sqw_map[qpt, :], label='simphony_pdata')
plt.plot(data[qpt, :], label='ab2tds')
plt.plot(data_i[qpt, :], label='ab2tds interpolation')
plt.title('Energy bin intensities at ' + str(idata.qpts[qpt]))
plt.legend()
f4.show()

ebins = np.arange(-100, 100.9, 0.9)
sqw_map(idata, ebins, sl, dw_grid=[4,4,4], calc_bose=True, T=0)
fig, ims = plot_sqw_map(idata, ratio=1.0, vmax=1e-8)
fig.show()
