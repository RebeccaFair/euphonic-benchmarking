import numpy as np
APPLYTIMEREVERSAL=0
Temperature=100
resolutionfile='resolution.txt'
Saturation=0.5
lowerLimit=0
bottom_meV=0
NEUTRONCALC=1
CohB={'La': 8.24, 'Zr': 7.16, 'O': 5.803}
NeutronE=100
branchWeight=np.ones(66).tolist()
EigScal=2
UniqueIon=-1

Nfour_interp=8
tth_max=45
redStarts = [[-1.0, 9.35, 3.35]]
redEnds = [[-1.0, -0.45, -6.45]]
Nqlines = [196]
