import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal


output_file = 'resolution.txt'
M = 101
std_dev = 12

if len(sys.argv) > 1:
    std_dev = float(sys.argv[1])
if len(sys.argv) > 2:
    M = int(sys.argv[2])

if std_dev == 0:
    res_func = np.zeros(M)
    res_func[(M - 1)/2] = 1
else:
    res_func = signal.gaussian(M, std_dev)


with open(output_file, 'w') as f:
    for i in range(len(res_func)):
        f.write('{: .0f} {: .6e}\n'.format(i - (M-1)/2, res_func[i]))

plt.plot(res_func)
plt.show()
