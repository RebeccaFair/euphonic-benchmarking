+-----------------------------------------------------------------------------+
|     Subroutine                              Total      Profiled   Time      |
|                                             calls      calls      (incl.)   |
+-----------------------------------------------------------------------------+
|   o-- <parent(s) not traced>                      1                         |
|  /                                                                          |
| O-> castep                                        1           1    865.28s  |
|    /                                                                        |
|   o-> memory_system_initialise                    1           1      0.00s  |
|   o-> comms_gcopy_real                            1           1      0.00s  |
|   o-> cell_read_wrapped                           1           1      3.93s  |
|   o-> ion_read                                    1           1      0.00s  |
|   o-> parameters_read                             1           1      0.02s  |
|   o-> bib_add                                     4           4      0.00s  |
|   o-> tddft_set_tddft_on                          1           1      0.00s  |
|   o-> model_continuation                          1           1     38.93s  |
|   o-> nlxc_initialise                             1           1      0.00s  |
|   o-> parameters_output                           1           1      0.01s  |
|   o-> cell_output_wrapped                         1           1      0.01s  |
|   o-> phonon_require_gs_wvfn                      1           1      0.00s  |
|   o-> phonon_calculate                            1           1    821.70s  |
|   o-> check_forces_stresses                       1           1      0.00s  |
|   o-> model_write                                 1           1      0.50s  |
|   o-> write_structure                             1           1      0.15s  |
|   o-> bib_output                                  1           1      0.01s  |
|   o-> model_deallocate                            1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /                                                                          |
| O-> phonon_calculate                              1           1    821.70s  |
|    /                                                                        |
|   o-> phonon_initialise                           1           1      0.00s  |
|   o-> secondd_symmetry_init                       1           1      0.00s  |
|   o-> phonon_require_gs_wvfn                      1           1      0.00s  |
|   o-> comms_reduce_kp_logical                     1           1      0.00s  |
|   o-> comms_reduce_bnd_logical                    1           1      0.00s  |
|   o-> bib_add                                     2           2      0.00s  |
|   o-> cell_copy                                   2           2      0.00s  |
|   o-> cell_rationalise_coordinates                1           1      0.00s  |
|   o-> secondd_compare_kpoints                     1           1      0.00s  |
|   o-> phonon_write_header                         2           2      2.34s  |
|   o-> cell_num_supercells                         1           1      0.00s  |
|   o-> phonon_print_force_const_matrix             1           1      0.02s  |
|   o-> q_is_a_g                                12224       12224      0.03s  |
|   o-> phonon_real_to_fine                     12215       12215     97.25s  |
|   o-> phonon_enforce_dmat_sumrule             12214       12214    174.36s  |
|   o-> phonon_prepare_and_diagonalise          12214       12214     37.75s  |
|   o-> phonon_accumulate_adp                   12214       12214      0.75s  |
|   o-> phonon_diag_and_output                  12214       12214    508.13s  |
|   o-> phonon_choose_gamma_directions            123         123      0.00s  |
|   o-> comms_reduce_gv_real                        1           1      0.00s  |
|   o-> comms_reduce_bnd_real                       1           1      0.00s  |
|   o-> comms_reduce_kp_real                        1           1      0.00s  |
|   o-> secondd_finalise                            1           1      0.00s  |
|   o-> cell_deallocate                             1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                        12214                         |
|  /                                                                          |
| O-> phonon_diag_and_output                    12214       12214    508.13s  |
|    /                                                                        |
|   o-> q_is_a_g                                24428       24428      0.06s  |
|   o-> phonon_prepare_and_diagonalise          12214       12214    196.04s  |
|   o-> secondd_calc_irreps                     12214       12214     84.64s  |
|   o-> phonon_write                            24428       24428    203.24s  |
|   o-> phonon_print_irreps                     12214       12214     23.10s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                        12214                         |
|  /o-- phonon_diag_and_output                  12214                         |
| |/                                                                          |
| O-> phonon_prepare_and_diagonalise            24428       24428    233.79s  |
|    /                                                                        |
|   o-> q_is_a_g                                48856       48856      0.11s  |
|   o-> secondd_symmetrize_dmat_cmplx           24428       24428     35.14s  |
|   o-> phonon_constrain_dmat                   24428       24428      0.07s  |
|   o-> phonon_mass_weight_dmat                 24428       24428      1.19s  |
|   o-> phonon_diagonalise_d_cmplx              24428       24428     36.29s  |
|   o-> phonon_print_dynamical_matrix           12214       12214    157.73s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_diag_and_output                  24428                         |
|  /                                                                          |
| O-> phonon_write                              24428       24428    203.24s  |
|    /                                                                        |
|   o-> q_is_a_g                                24428       24428      0.06s  |
|   o-> phonon_write_freqs                      24428       24428     18.63s  |
|   o-> secondd_find_acoustics                    246         246      0.03s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                        12214                         |
|  /                                                                          |
| O-> phonon_enforce_dmat_sumrule               12214       12214    174.36s  |
|    /                                                                        |
|   o-> phonon_mass_weight_dmat                 24428       24428      1.19s  |
|   o-> phonon_diagonalise_d_cmplx              36642       36642     55.72s  |
|   o-> secondd_find_acoustics                  12214       12214      1.73s  |
|   o-> secondd_symmetrize_dmat_cmplx           12214       12214    112.82s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_real_to_fine                     12215                         |
|  /o-- phonon_enforce_dmat_sumrule             12214                         |
|  /o-- phonon_prepare_and_diagonalise          24428                         |
| |/                                                                          |
| O-> secondd_symmetrize_dmat_cmplx             48857       48857    165.48s  |
|    /                                                                        |
|   o-> two_q_is_a_g                            48857       48857      0.11s  |
|   o-> hermitian4                              48857       48857      0.46s  |
|   o-> cell_copy_symmetry                      97714       97714      1.32s  |
|   o-> cell_reduce_symmetry_ops_wrapped        48857       48857     17.61s  |
|   o-> secondd_group_symmetrize_dmat           48857       48857    127.73s  |
|   o-> cell_deallocate                         48857       48857      0.23s  |
|   o-> secondd_find_inversion                  48857       48857      0.14s  |
|   o-> secondd_tr_symmetrize_dmat              48857       48857     16.32s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_prepare_and_diagonalise          12214                         |
|  /                                                                          |
| O-> phonon_print_dynamical_matrix             12214       12214    157.73s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_symmetrize_dmat_cmplx           48857                         |
|  /                                                                          |
| O-> secondd_group_symmetrize_dmat             48857       48857    127.73s  |
|    /                                                                        |
|   o-> cell_factor_group_symmetry_wrapped      48857       48857      0.59s  |
|   o-> secondd_check_equiv_ions                48857       48857      0.82s  |
|   o-> secondd_apply_symmetry_to_dmat_ion     789485      789485    112.85s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_group_symmetrize_dmat          789485                         |
|  /o-- secondd_tr_symmetrize_dmat              48857                         |
| |/                                                                          |
| O-> secondd_apply_symmetry_to_dmat_ion       838342      838342    119.93s  |
|    /                                                                        |
|   o-> secondd_compute_atom_phases            838342      838342     35.56s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                        12215                         |
|  /                                                                          |
| O-> phonon_real_to_fine                       12215       12215     97.25s  |
|    /                                                                        |
|   o-> cell_num_supercells                     12215       12215      0.03s  |
|   o-> cell_rationalise_coordinates            12215       12215      0.05s  |
|   o-> cell_supercell                          12215       12215     76.21s  |
|   o-> check_image_list                        12215       12215      0.04s  |
|   o-> secondd_symmetrize_dmat_cmplx           12215       12215     17.52s  |
|   o-> cell_deallocate                         12215       12215      0.14s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_enforce_dmat_sumrule             36642                         |
|  /o-- phonon_prepare_and_diagonalise          24428                         |
| |/                                                                          |
| O-> phonon_diagonalise_d_cmplx                61070       61070     92.01s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_diag_and_output                  12214                         |
|  /                                                                          |
| O-> secondd_calc_irreps                       12214       12214     84.64s  |
|    /                                                                        |
|   o-> cell_copy_symmetry                      24428       24428      0.33s  |
|   o-> cell_reduce_symmetry_ops_wrapped        12214       12214      0.81s  |
|   o-> cell_check_group                        12214       12214      0.47s  |
|   o-> cell_factor_group_symmetry_wrapped      12214       12214      0.04s  |
|   o-> cell_symmetry_symbol_wrapped_wrapped    87611       87611      0.66s  |
|   o-> secondd_check_equiv_ions                12214       12214      0.30s  |
|   o-> secondd_compute_atom_phases             51436       51436      2.22s  |
|   o-> cell_conjugacy_classes_wrapped          12214       12214      0.35s  |
|   o-> cell_deallocate                         12214       12214      0.05s  |
|   o-> secondd_determinant                      5904        5904      0.01s  |
|   o-> secondd_analyse_symmetries                123         123      0.00s  |
|   o-> secondd_irrep_symbol                      984         984      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_real_to_fine                     12215                         |
|  /                                                                          |
| O-> cell_supercell                            12215       12215     76.21s  |
|    /                                                                        |
|   o-> cell_deallocate                         12215       12215      0.04s  |
|   o-> cell_num_supercells                     12215       12215      0.03s  |
|   o-> algor_invert_real                       12215       12215      0.03s  |
|   o-> cell_allocate                           12215       12215      0.41s  |
|   o-> cell_recip_lattice                      12215       12215      0.04s  |
|   o-> cell_calculate_volume                   12215       12215      0.03s  |
|   o-> cell_generate_supercell_origins         12215       12215      0.16s  |
|   o-> cell_copy_kpoints                       12215       12215      1.42s  |
|   o-> cell_set_current_kpoints_wrapped        12215       12215      0.06s  |
|   o-> cell_set_supercell_symmetry             12215       12215     37.76s  |
|   o-> cell_generate_cell_constraints          12215       12215      0.51s  |
|   o-> cell_generate_ionic_constraints         12215       12215     35.06s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /                                                                          |
| O-> model_continuation                            1           1     38.93s  |
|    /                                                                        |
|   o-> model_reset                                 1           1      0.00s  |
|   o-> comms_gcopy_logical                         4           4      0.00s  |
|   o-> parameters_restore                          1           1      0.00s  |
|   o-> cell_restore_wrapped                        2           2      0.45s  |
|   o-> comms_parallel_strategy                     1           1      0.00s  |
|   o-> cell_allocate                               2           2      0.00s  |
|   o-> cell_copy                                   4           4      0.00s  |
|   o-> cell_distribute_kpoints_wrapped             3           3      0.00s  |
|   o-> ion_initialise                              1           1     31.87s  |
|   o-> basis_initialise                            2           2      0.10s  |
|   o-> model_store_dependencies                    1           1      0.00s  |
|   o-> cell_reread_wrapped                         1           1      2.69s  |
|   o-> comms_gcopy_real                            7           7      0.00s  |
|   o-> parameters_nspins                           3           3      0.00s  |
|   o-> comms_gcopy_integer                         5           5      0.00s  |
|   o-> wave_allocate_wv                            1           1      0.00s  |
|   o-> wave_initialise_wv                          1           1      3.76s  |
|   o-> model_read_occ_eigenvalues                  1           1      0.00s  |
|   o-> density_allocate                            1           1      0.00s  |
|   o-> density_read                                1           1      0.04s  |
|   o-> comms_gcopy_character                       6           6      0.00s  |
|   o-> cell_num_supercells                         1           1      0.00s  |
|   o-> parameters_reread                           1           1      0.02s  |
|   o-> model_check_dependencies                    1           1      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_apply_symmetry_to_dmat_ion     838342                         |
|  /o-- secondd_calc_irreps                     51436                         |
| |/                                                                          |
| O-> secondd_compute_atom_phases              889778      889778     37.78s  |
|    /                                                                        |
|   o-> q_is_a_g                               889778      889778      1.96s  |
+-----------------------------------------------------------------------------+
|   o-- cell_supercell                          12215                         |
|  /                                                                          |
| O-> cell_set_supercell_symmetry               12215       12215     37.76s  |
|    /                                                                        |
|   o-> cell_num_supercells                     12215       12215      0.03s  |
|   o-> algor_invert_real                       12215       12215      0.03s  |
|   o-> cell_reduce_symmetry_supercell          12215       12215      2.42s  |
|   o-> cell_find_related_atoms_supercell       12215       12215     24.15s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_supercell                          12215                         |
| |/                                                                          |
| O-> cell_generate_ionic_constraints           12216       12216     35.06s  |
|    /                                                                        |
|   o-> algor_uniform_random                  6156486     6156486     13.09s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_output_wrapped                         1                         |
|  /o-- cell_reduce_symmetry_ops_wrapped        61071                         |
|  /o-- secondd_calc_irreps                     12214                         |
|  /o-- phonon_print_irreps                     12214                         |
| |/                                                                          |
| O-> cell_check_group                          85501       85501     32.12s  |
|    /                                                                        |
|   o-> algor_invert_real                     1429868     1429868      3.22s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          1                         |
|  /                                                                          |
| O-> ion_initialise                                1           1     31.87s  |
|    /                                                                        |
|   o-> ion_allocate                                1           1      0.00s  |
|   o-> ion_atom_allocate_pspot                     6           6      0.15s  |
|   o-> ion_atom_generate_psp                       3           3     30.33s  |
|   o-> ion_set_data                                3           3      0.10s  |
|   o-> ion_atom_deallocate_pspot                   7           7      0.00s  |
|   o-> ion_set_psp                                 3           3      0.06s  |
|   o-> ion_atom_pseudo_scf                         3           3      1.22s  |
|   o-> ion_clebsch_gordan                          1           1      0.00s  |
|   o-> ion_atom_radial_transform                   1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_initialise                              3                         |
|  /                                                                          |
| O-> ion_atom_generate_psp                         3           3     30.33s  |
|    /                                                                        |
|   o-> ion_atom_init_ae_basis                      3           3      0.00s  |
|   o-> ion_atom_init_ae_atom                       3           3      0.00s  |
|   o-> ion_atom_ae_scf                             3           3      1.03s  |
|   o-> ion_atom_define_psp                         3           3      1.21s  |
|   o-> ion_atom_construct_psp_paw                  3           3     27.10s  |
|   o-> ion_atom_descreen_psp                       3           3      0.36s  |
|   o-> ion_atom_output_psp                         3           3      0.64s  |
|   o-> ion_atom_basis_ae_dealloc                   3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_generate_psp                       3                         |
|  /                                                                          |
| O-> ion_atom_construct_psp_paw                    3           3     27.10s  |
|    /                                                                        |
|   o-> ion_atom_partial_wave                      21          21      0.00s  |
|   o-> ion_atom_pseudise                          21          21      0.07s  |
|   o-> ion_atom_write_pwave                        3           3      0.00s  |
|   o-> ion_atom_radin                          61185       61185      0.83s  |
|   o-> ion_atom_Q_pseudise                        95          95      9.32s  |
|   o-> ion_atom_write_beta                         3           3      0.00s  |
|   o-> ion_atom_radial_transform                  92          92      5.08s  |
|   o-> ion_atom_fermi_contact                     44          44      0.00s  |
|   o-> ion_atom_apply_Tl                          84          84      0.01s  |
|   o-> ion_atom_real_derivative_kind1            168         168      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- cell_set_supercell_symmetry             12215                         |
|  /                                                                          |
| O-> cell_find_related_atoms_supercell         12215       12215     24.15s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_diag_and_output                  12214                         |
|  /                                                                          |
| O-> phonon_print_irreps                       12214       12214     23.10s  |
|    /                                                                        |
|   o-> cell_check_group                        12214       12214     14.83s  |
|   o-> cell_factor_group_symmetry_wrapped      12214       12214      0.47s  |
|   o-> cell_symmetry_symbol_wrapped_wrapped   586272      586272      4.30s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_write                            24428                         |
|  /                                                                          |
| O-> phonon_write_freqs                        24428       24428     18.63s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_symmetrize_dmat_cmplx           48857                         |
|  /o-- secondd_calc_irreps                     12214                         |
| |/                                                                          |
| O-> cell_reduce_symmetry_ops_wrapped          61071       61071     18.42s  |
|    /                                                                        |
|   o-> cell_check_group                        61071       61071     16.83s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_symmetrize_dmat_cmplx           48857                         |
|  /                                                                          |
| O-> secondd_tr_symmetrize_dmat                48857       48857     16.32s  |
|    /                                                                        |
|   o-> secondd_check_equiv_ions                48857       48857      8.40s  |
|   o-> secondd_apply_symmetry_to_dmat_ion      48857       48857      7.08s  |
+-----------------------------------------------------------------------------+
|   o-- cell_generate_ionic_constraints       6156486                         |
|  /                                                                          |
| O-> algor_uniform_random                    6156486     6156486     13.09s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_group_symmetrize_dmat           48857                         |
|  /o-- secondd_tr_symmetrize_dmat              48857                         |
|  /o-- secondd_calc_irreps                     12214                         |
| |/                                                                          |
| O-> secondd_check_equiv_ions                 109928      109928      9.53s  |
|    /                                                                        |
|   o-> secondd_equiv_atoms_to_ions           1901064     1901064      4.33s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_construct_psp_paw                 95                         |
|  /                                                                          |
| O-> ion_atom_Q_pseudise                          95          95      9.32s  |
|    /                                                                        |
|   o-> ion_atom_set_qn                            13          13      0.02s  |
|   o-> ion_atom_radin                          77045       77045      0.75s  |
|   o-> ion_atom_poly_deriv                        95          95      0.00s  |
|   o-> ion_atom_regin                           6840        6840      0.02s  |
|   o-> ion_atom_solve_linear                      95          95      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_define_psp                         3                         |
|  /o-- ion_atom_construct_psp_paw                 92                         |
|  /o-- ion_initialise                              1                         |
| |/                                                                          |
| O-> ion_atom_radial_transform                    96          96      6.08s  |
+-----------------------------------------------------------------------------+
|   o-- cell_output_wrapped                        48                         |
|  /o-- secondd_calc_irreps                     87611                         |
|  /o-- phonon_print_irreps                    586272                         |
|  /o-- cell_write_cell                            48                         |
| |/                                                                          |
| O-> cell_symmetry_symbol_wrapped_wrapped     673979      673979      4.96s  |
|    /                                                                        |
|   o-> algor_invert_real                      673979      673979      1.57s  |
|   o-> cell_cart_lattice_to_abc                   48          48      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_find_reduced_cell                    248                         |
|  /o-- cell_sort_kpoints_with_recip              494                         |
|  /o-- cell_reduce_kpoints_internal                2                         |
|  /o-- cell_check_group                      1429868                         |
|  /o-- ion_atom_pn_pseudise                        3                         |
|  /o-- cell_symmetry_symbol_wrapped_wrapped   673979                         |
|  /o-- cell_supercell                          12215                         |
|  /o-- cell_generate_supercell_origins         12215                         |
|  /o-- cell_set_supercell_symmetry             12215                         |
| |/                                                                          |
| O-> algor_invert_real                       2141239     2141239      4.88s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_check_equiv_ions              1901064                         |
|  /                                                                          |
| O-> secondd_equiv_atoms_to_ions             1901064     1901064      4.33s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /                                                                          |
| O-> cell_read_wrapped                             1           1      3.93s  |
|    /                                                                        |
|   o-> cell_setup_keywords                         1           1      0.00s  |
|   o-> comms_gcopy_integer                        63          63      0.00s  |
|   o-> cell_check_keywords                         1           1      0.00s  |
|   o-> cell_read_line_real                        66          66      0.00s  |
|   o-> cell_read_line_char                        22          22      0.00s  |
|   o-> cell_allocate                               1           1      0.00s  |
|   o-> cell_calculate_volume                       2           2      0.00s  |
|   o-> cell_recip_lattice                          1           1      0.00s  |
|   o-> cell_analyse_symmetry_wrapped               1           1      0.22s  |
|   o-> cell_kpoints_mp_wrapped                     1           1      0.00s  |
|   o-> cell_read_optics_data                       1           1      0.00s  |
|   o-> cell_read_bs_data                           1           1      0.00s  |
|   o-> cell_read_spectral_data                     1           1      0.00s  |
|   o-> cell_read_phonon_data                       1           1      0.05s  |
|   o-> cell_read_phonon_fine_data                  1           1      0.36s  |
|   o-> cell_read_magres_data                       1           1      0.19s  |
|   o-> cell_read_elnes_data                        1           1      0.14s  |
|   o-> cell_read_supercell_data                    1           1      0.00s  |
|   o-> cell_generate_cell_constraints              1           1      0.00s  |
|   o-> cell_check_cell_constraints                 1           1      0.00s  |
|   o-> cell_find_related_atoms                     1           1      0.00s  |
|   o-> cell_detect_primitive                       1           1      0.00s  |
|   o-> cell_check_group                            1           1      0.00s  |
|   o-> cell_count_symmetry_translations_wrap       1           1      0.00s  |
|   o-> cell_check_group_equiv                      1           1      0.00s  |
|   o-> cell_symmetry_test                          2           2      0.00s  |
|   o-> cell_check_symmetry_ops_spin                1           1      0.00s  |
|   o-> cell_generate_ionic_constraints             1           1      0.00s  |
|   o-> comms_gcopy_logical                        11          11      0.00s  |
|   o-> comms_gcopy_real                           68          68      0.00s  |
|   o-> comms_gcopy_character                      28          28      0.00s  |
|   o-> cell_generate_qpoints_local                 1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          1                         |
|  /                                                                          |
| O-> wave_initialise_wv                            1           1      3.76s  |
|     +- section "initialisation", branch on value of method:-                |
|       R                                           1           1      3.76s  |
|         \                                                                   |
|          o-> wave_spin_type_wv                    1           1      0.00s  |
|          o-> wave_prepare_init_wvfn               1           1      0.00s  |
|          o-> algor_set_random_seed                1           1      0.00s  |
|          o-> algor_uniform_random_array        1060        1060      0.02s  |
|          o-> wave_Sorthonormalise_wv              1           1      3.67s  |
|    /                                                                        |
|   o-> ion_set_projectors                          1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- wave_initialise_wv                          1                         |
|  /                                                                          |
| O-> wave_Sorthonormalise_wv                       1           1      3.67s  |
|    /                                                                        |
|   o-> wave_Sorthonormalise_wv_ks                 10          10      3.67s  |
+-----------------------------------------------------------------------------+
|   o-- wave_Sorthonormalise_wv                    10                         |
|  /                                                                          |
| O-> wave_Sorthonormalise_wv_ks                   10          10      3.67s  |
|    /                                                                        |
|   o-> wave_calc_Soverlap_wv_ks                   10          10      3.26s  |
|   o-> wave_orthonormalise_over_wv_ks             10          10      0.42s  |
+-----------------------------------------------------------------------------+
|   o-- wave_Sorthonormalise_wv_ks                 10                         |
|  /                                                                          |
| O-> wave_calc_Soverlap_wv_ks                     10          10      3.26s  |
|    /                                                                        |
|   o-> coeffs_dot_all_self                        10          10      0.44s  |
|   o-> wave_beta_phi_wv_ks                        10          10      2.79s  |
|   o-> wave_q_dot_all_self_c                      10          10      0.03s  |
|   o-> comms_reduce_gv_complex                    10          10      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- wave_calc_Soverlap_wv_ks                   10                         |
|  /                                                                          |
| O-> wave_beta_phi_wv_ks                          10          10      2.79s  |
|    /                                                                        |
|   o-> ion_set_projectors                         10          10      0.00s  |
|   o-> wave_calc_ps_q_nonzero                     10          10      0.00s  |
|   o-> ion_all_beta_multi_phi_recip               10          10      2.71s  |
+-----------------------------------------------------------------------------+
|   o-- wave_beta_phi_wv_ks                        10                         |
|  /                                                                          |
| O-> ion_all_beta_multi_phi_recip                 10          10      2.71s  |
|    /                                                                        |
|   o-> ion_beta_recip_set                         10          10      1.22s  |
|   o-> algor_matmul_cmplx_cmplx                   10          10      1.48s  |
|   o-> comms_reduce_gv_complex                    10          10      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          1                         |
|  /                                                                          |
| O-> cell_reread_wrapped                           1           1      2.69s  |
|    /                                                                        |
|   o-> cell_setup_keywords                         1           1      0.00s  |
|   o-> comms_gcopy_integer                        59          59      0.00s  |
|   o-> cell_check_keywords                         1           1      0.00s  |
|   o-> cell_read_optics_data                       1           1      0.00s  |
|   o-> cell_read_bs_data                           1           1      0.00s  |
|   o-> cell_read_spectral_data                     1           1      0.00s  |
|   o-> cell_read_phonon_data                       1           1      0.05s  |
|   o-> cell_read_phonon_fine_data                  1           1      0.35s  |
|   o-> cell_read_magres_data                       1           1      0.19s  |
|   o-> cell_read_elnes_data                        1           1      0.14s  |
|   o-> cell_read_supercell_data                    1           1      0.00s  |
|   o-> cell_generate_cell_constraints              1           1      0.00s  |
|   o-> cell_check_cell_constraints                 1           1      0.00s  |
|   o-> comms_gcopy_real                           66          66      0.00s  |
|   o-> comms_gcopy_logical                        10          10      0.00s  |
|   o-> comms_gcopy_character                      28          28      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_set_supercell_symmetry             12215                         |
|  /                                                                          |
| O-> cell_reduce_symmetry_supercell            12215       12215      2.42s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_enforce_dmat_sumrule             24428                         |
|  /o-- phonon_prepare_and_diagonalise          24428                         |
| |/                                                                          |
| O-> phonon_mass_weight_dmat                   48856       48856      2.38s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                            2                         |
|  /                                                                          |
| O-> phonon_write_header                           2           2      2.34s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                        12224                         |
|  /o-- secondd_compute_atom_phases            889778                         |
|  /o-- phonon_prepare_and_diagonalise          48856                         |
|  /o-- phonon_diag_and_output                  24428                         |
|  /o-- phonon_write                            24428                         |
|  /o-- phonon_choose_gamma_directions            123                         |
| |/                                                                          |
| O-> q_is_a_g                                 999837      999837      2.22s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_enforce_dmat_sumrule             12214                         |
|  /o-- phonon_write                              246                         |
| |/                                                                          |
| O-> secondd_find_acoustics                    12460       12460      1.77s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_koelling_harmon_solve           4778                         |
|  /o-- ion_atom_ae_hartree                       624                         |
|  /o-- ion_atom_ae_xc                            208                         |
|  /o-- ion_atom_define_psp                         9                         |
|  /o-- ion_atom_core_pseudise                   2406                         |
|  /o-- ion_atom_kh_partial_wave                   21                         |
|  /o-- ion_atom_construct_psp_paw              61185                         |
|  /o-- ion_atom_Q_pseudise                     77045                         |
|  /o-- ion_atom_ps_solve                        1322                         |
|  /o-- ion_atom_descreen_psp                    6167                         |
| |/                                                                          |
| O-> ion_atom_radin                           153765      153765      1.77s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_symmetrize_dmat_cmplx           97714                         |
|  /o-- secondd_calc_irreps                     24428                         |
| |/                                                                          |
| O-> cell_copy_symmetry                       122142      122142      1.65s  |
+-----------------------------------------------------------------------------+
|   o-- ion_all_beta_multi_phi_recip               10                         |
|  /                                                                          |
| O-> algor_matmul_cmplx_cmplx                     10          10      1.48s  |
+-----------------------------------------------------------------------------+
|   o-- cell_supercell                          12215                         |
|  /                                                                          |
| O-> cell_copy_kpoints                         12215       12215      1.42s  |
+-----------------------------------------------------------------------------+
|   o-- ion_initialise                              3                         |
|  /                                                                          |
| O-> ion_atom_pseudo_scf                           3           3      1.22s  |
|    /                                                                        |
|   o-> ion_atom_init_pseudo_basis                  3           3      0.02s  |
|   o-> ion_atom_init_pseudo_atom                   3           3      0.29s  |
|   o-> ion_atom_init_pseudo_H                      3           3      0.00s  |
|   o-> ion_atom_ps_diag                          108         108      0.77s  |
|   o-> ion_atom_set_pseudo_H                     108         108      0.03s  |
|   o-> ion_atom_regin                           6003        6003      0.01s  |
|   o-> ion_atom_basis_pseudo_dealloc               3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_all_beta_multi_phi_recip               10                         |
|  /                                                                          |
| O-> ion_beta_recip_set                           10          10      1.22s  |
|    /                                                                        |
|   o-> ion_set_projectors                         10          10      0.00s  |
|   o-> ion_cc_structure_factor                   220         220      0.04s  |
|   o-> ion_beta_recip_interpolation             2640        2640      0.57s  |
|   o-> basis_multiply_recip_reduced             2640        2640      0.06s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_generate_psp                       3                         |
|  /                                                                          |
| O-> ion_atom_define_psp                           3           3      1.21s  |
|    /                                                                        |
|   o-> ion_atom_round_to_grid                     69          69      0.00s  |
|   o-> ion_atom_radin                              9           9      0.00s  |
|   o-> ion_atom_core_pseudise                      3           3      0.21s  |
|   o-> ion_atom_radial_transform                   3           3      1.00s  |
|   o-> ion_atom_derivative                         3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_count_symmetry_translations_wrap       1                         |
|  /o-- cell_output_wrapped                         1                         |
|  /o-- secondd_group_symmetrize_dmat           48857                         |
|  /o-- secondd_calc_irreps                     12214                         |
|  /o-- cell_conjugacy_classes_wrapped          12214                         |
|  /o-- phonon_print_irreps                     12214                         |
| |/                                                                          |
| O-> cell_factor_group_symmetry_wrapped        85501       85501      1.14s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_generate_psp                       3                         |
|  /                                                                          |
| O-> ion_atom_ae_scf                               3           3      1.03s  |
|    /                                                                        |
|   o-> ion_atom_init_ae_H                          3           3      0.00s  |
|   o-> ion_atom_ae_solve                         202         202      0.95s  |
|   o-> ion_atom_set_ae_H                         205         205      0.08s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_ae_scf                           202                         |
|  /                                                                          |
| O-> ion_atom_ae_solve                           202         202      0.95s  |
|    /                                                                        |
|   o-> ion_atom_koelling_harmon_solve            202         202      0.95s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_ae_solve                         202                         |
|  /                                                                          |
| O-> ion_atom_koelling_harmon_solve              202         202      0.95s  |
|    /                                                                        |
|   o-> ion_atom_cfd_solve                       9896        9896      0.57s  |
|   o-> ion_atom_number_of_nodes                 4948        4948      0.02s  |
|   o-> ion_atom_radin                           4778        4778      0.06s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_pseudo_scf                       108                         |
|  /                                                                          |
| O-> ion_atom_ps_diag                            108         108      0.77s  |
|    /                                                                        |
|   o-> ion_atom_regin                         139221      139221      0.32s  |
|   o-> ion_atom_rectoreal                        354         354      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                        12214                         |
|  /                                                                          |
| O-> phonon_accumulate_adp                     12214       12214      0.75s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_reread_wrapped                         1                         |
| |/                                                                          |
| O-> cell_read_phonon_fine_data                    2           2      0.71s  |
|    /                                                                        |
|   o-> cell_detect_MP                              2           2      0.17s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_restore_wrapped                        2                         |
| |/                                                                          |
| O-> cell_analyse_symmetry_wrapped                 3           3      0.66s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_generate_psp                       3                         |
|  /                                                                          |
| O-> ion_atom_output_psp                           3           3      0.64s  |
|    /                                                                        |
|   o-> algor_sort                                  3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_koelling_harmon_solve           9896                         |
|  /o-- ion_atom_kh_partial_wave                   21                         |
| |/                                                                          |
| O-> ion_atom_cfd_solve                         9917        9917      0.57s  |
+-----------------------------------------------------------------------------+
|   o-- ion_beta_recip_set                       2640                         |
|  /                                                                          |
| O-> ion_beta_recip_interpolation               2640        2640      0.57s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_reread_wrapped                         1                         |
|  /o-- cell_supercell                          12215                         |
| |/                                                                          |
| O-> cell_generate_cell_constraints            12217       12217      0.51s  |
|    /                                                                        |
|   o-> algor_uniform_random_array              12217       12217      0.03s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /                                                                          |
| O-> model_write                                   1           1      0.50s  |
|    /                                                                        |
|   o-> model_write_all                             1           1      0.50s  |
|   o-> io_delete_file                              1           1      0.00s  |
|   o-> comms_barrier_farm                          1           1      0.00s  |
|   o-> comms_barrier                               1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_write                                 1                         |
|  /                                                                          |
| O-> model_write_all                               1           1      0.50s  |
|    /                                                                        |
|   o-> parameters_dump                             1           1      0.00s  |
|   o-> cell_dump                                   2           2      0.01s  |
|   o-> model_write_occ_eigenvalues                 1           1      0.00s  |
|   o-> density_write                               1           1      0.24s  |
+-----------------------------------------------------------------------------+
|   o-- cell_allocate                           12220                         |
|  /o-- model_reset                                 8                         |
|  /o-- cell_restore_cell                           2                         |
|  /o-- cell_supercell                          12215                         |
|  /o-- secondd_symmetrize_dmat_cmplx           48857                         |
|  /o-- phonon_real_to_fine                     12215                         |
|  /o-- secondd_calc_irreps                     12214                         |
|  /o-- phonon_calculate                            1                         |
| |/                                                                          |
| O-> cell_deallocate                           97732       97732      0.50s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_symmetrize_dmat_cmplx           48857                         |
|  /                                                                          |
| O-> hermitian4                                48857       48857      0.46s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          2                         |
|  /                                                                          |
| O-> cell_restore_wrapped                          2           2      0.45s  |
|    /                                                                        |
|   o-> comms_gcopy_character                       2           2      0.00s  |
|   o-> cell_restore_cell                           2           2      0.00s  |
|   o-> cell_restore_global_wrapped                 2           2      0.00s  |
|   o-> cell_analyse_symmetry_wrapped               2           2      0.44s  |
+-----------------------------------------------------------------------------+
|   o-- wave_calc_Soverlap_wv_ks                   10                         |
|  /                                                                          |
| O-> coeffs_dot_all_self                          10          10      0.44s  |
|    /                                                                        |
|   o-> local_dot_all_self                         10          10      0.44s  |
+-----------------------------------------------------------------------------+
|   o-- coeffs_dot_all_self                        10                         |
|  /                                                                          |
| O-> local_dot_all_self                           10          10      0.44s  |
+-----------------------------------------------------------------------------+
|   o-- wave_Sorthonormalise_wv_ks                 10                         |
|  /                                                                          |
| O-> wave_orthonormalise_over_wv_ks               10          10      0.42s  |
|    /                                                                        |
|   o-> comms_reduce_bnd_complex                   10          10      0.00s  |
|   o-> algor_invert_complex                       10          10      0.01s  |
|   o-> comms_copy_gv_complex                      10          10      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_restore_cell                           2                         |
|  /o-- model_continuation                          2                         |
|  /o-- cell_supercell                          12215                         |
| |/                                                                          |
| O-> cell_allocate                             12220       12220      0.41s  |
|    /                                                                        |
|   o-> cell_deallocate                         12220       12220      0.03s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_reread_wrapped                         1                         |
| |/                                                                          |
| O-> cell_read_magres_data                         2           2      0.37s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_core_pseudise                    168                         |
|  /o-- ion_atom_qc_pseudise_fn                  1296                         |
|  /o-- ion_atom_Q_pseudise                      6840                         |
|  /o-- ion_atom_init_pseudo_basis                366                         |
|  /o-- ion_atom_init_pseudo_H                    117                         |
|  /o-- ion_atom_pseudo_hartree                   111                         |
|  /o-- ion_atom_pseudo_xc                        111                         |
|  /o-- ion_atom_ps_diag                       139221                         |
|  /o-- ion_atom_calc_pseudo_rho                  108                         |
|  /o-- ion_atom_set_pseudo_H                    4113                         |
|  /o-- ion_atom_pseudo_scf                      6003                         |
| |/                                                                          |
| O-> ion_atom_regin                           158454      158454      0.37s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_generate_psp                       3                         |
|  /                                                                          |
| O-> ion_atom_descreen_psp                         3           3      0.36s  |
|    /                                                                        |
|   o-> ion_atom_init_ae_atom                       3           3      0.00s  |
|   o-> ion_atom_set_ae_occ                         3           3      0.00s  |
|   o-> ion_atom_ps_solve                           3           3      0.06s  |
|   o-> ion_atom_radin                           6167        6167      0.08s  |
|   o-> ion_atom_ae_hartree                         3           3      0.00s  |
|   o-> ion_atom_ae_xc                              3           3      0.00s  |
|   o-> ion_atom_derivative                         3           3      0.00s  |
|   o-> ion_atom_ae_dealloc                         3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_calc_irreps                     12214                         |
|  /                                                                          |
| O-> cell_conjugacy_classes_wrapped            12214       12214      0.35s  |
|    /                                                                        |
|   o-> cell_factor_group_symmetry_wrapped      12214       12214      0.04s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_pseudo_scf                         3                         |
|  /                                                                          |
| O-> ion_atom_init_pseudo_atom                     3           3      0.29s  |
|    /                                                                        |
|   o-> ion_atom_resolve_pseudo_cfg                 3           3      0.00s  |
|   o-> ion_atom_locate                         12410       12410      0.03s  |
|   o-> ion_atom_interpolate                    12410       12410      0.03s  |
|   o-> ion_atom_set_pseudo_occ                     3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_reread_wrapped                         1                         |
| |/                                                                          |
| O-> cell_read_elnes_data                          2           2      0.28s  |
+-----------------------------------------------------------------------------+
|   o-- model_write_all                             1                         |
|  /                                                                          |
| O-> density_write                                 1           1      0.24s  |
|    /                                                                        |
|   o-> density_allocate                            1           1      0.00s  |
|   o-> density_copy                                1           1      0.00s  |
|   o-> density_real_to_complex                     1           1      0.01s  |
|   o-> density_write_serial                        1           1      0.23s  |
|   o-> density_deallocate                          1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- density_write                               1                         |
|  /                                                                          |
| O-> density_write_serial                          1           1      0.23s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_define_psp                         3                         |
|  /                                                                          |
| O-> ion_atom_core_pseudise                        3           3      0.21s  |
|    /                                                                        |
|   o-> ion_atom_set_qn                             3           3      0.00s  |
|   o-> ion_atom_poly_deriv                         3           3      0.00s  |
|   o-> ion_atom_radin                           2406        2406      0.02s  |
|   o-> ion_atom_regin                            168         168      0.00s  |
|   o-> ion_atom_solve_linear                       3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_phonon_fine_data                  2                         |
|  /o-- cell_generate_qpoints_local                 2                         |
|  /o-- cell_restore_global_wrapped                 4                         |
| |/                                                                          |
| O-> cell_detect_MP                                8           8      0.17s  |
+-----------------------------------------------------------------------------+
|   o-- cell_supercell                          12215                         |
|  /                                                                          |
| O-> cell_generate_supercell_origins           12215       12215      0.16s  |
|    /                                                                        |
|   o-> cell_num_supercells                     12215       12215      0.03s  |
|   o-> algor_invert_real                       12215       12215      0.03s  |
+-----------------------------------------------------------------------------+
|   o-- ion_initialise                              6                         |
|  /                                                                          |
| O-> ion_atom_allocate_pspot                       6           6      0.15s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /                                                                          |
| O-> write_structure                               1           1      0.15s  |
|    /                                                                        |
|   o-> cell_write_cell                             1           1      0.12s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_symmetrize_dmat_cmplx           48857                         |
|  /                                                                          |
| O-> secondd_find_inversion                    48857       48857      0.14s  |
+-----------------------------------------------------------------------------+
|   o-- write_structure                             1                         |
|  /                                                                          |
| O-> cell_write_cell                               1           1      0.12s  |
|    /                                                                        |
|   o-> cell_symmetry_symbol_wrapped_wrapped       48          48      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_symmetrize_dmat_cmplx           48857                         |
|  /                                                                          |
| O-> two_q_is_a_g                              48857       48857      0.11s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          1                         |
|  /o-- phonon_calculate                            1                         |
|  /o-- phonon_real_to_fine                     12215                         |
|  /o-- cell_supercell                          12215                         |
|  /o-- cell_generate_supercell_origins         12215                         |
|  /o-- cell_set_supercell_symmetry             12215                         |
| |/                                                                          |
| O-> cell_num_supercells                       48862       48862      0.11s  |
+-----------------------------------------------------------------------------+
|   o-- ion_initialise                              3                         |
|  /                                                                          |
| O-> ion_set_data                                  3           3      0.10s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          2                         |
|  /                                                                          |
| O-> basis_initialise                              2           2      0.10s  |
|    /                                                                        |
|   o-> basis_utils_prime_factors                   6           6      0.00s  |
|   o-> basis_set_serial_grids                      1           1      0.00s  |
|   o-> basis_map_standard_to_fine                  1           1      0.00s  |
|   o-> basis_map_fine_recip_half_full              1           1      0.00s  |
|   o-> basis_assign_grid_coordinates               2           2      0.07s  |
|   o-> basis_count_plane_waves                     1           1      0.01s  |
|   o-> basis_assign_plane_wave_indexes             1           1      0.01s  |
|   o-> basis_assign_pw_gvectors                    2           2      0.01s  |
|   o-> basis_calculate_cut_off                     2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_reread_wrapped                         1                         |
| |/                                                                          |
| O-> cell_read_phonon_data                         2           2      0.10s  |
|    /                                                                        |
|   o-> cell_kpoints_mp_wrapped                     2           2      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_ae_scf                           205                         |
|  /                                                                          |
| O-> ion_atom_set_ae_H                           205         205      0.08s  |
|    /                                                                        |
|   o-> ion_atom_ae_hartree                       205         205      0.02s  |
|   o-> ion_atom_ae_xc                            205         205      0.04s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_construct_psp_paw                 21                         |
|  /                                                                          |
| O-> ion_atom_pseudise                            21          21      0.07s  |
|    /                                                                        |
|   o-> ion_atom_pn_pseudise                        3           3      0.00s  |
|   o-> ion_atom_qc_pseudise                       18          18      0.07s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_pseudise                          18                         |
|  /                                                                          |
| O-> ion_atom_qc_pseudise                         18          18      0.07s  |
|    /                                                                        |
|   o-> ion_atom_qc_pseudise_fn                    18          18      0.07s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_qc_pseudise                       18                         |
|  /                                                                          |
| O-> ion_atom_qc_pseudise_fn                      18          18      0.07s  |
|    /                                                                        |
|   o-> ion_atom_set_qn                            18          18      0.01s  |
|   o-> ion_atom_poly_deriv                        18          18      0.00s  |
|   o-> ion_atom_expjl                           7218        7218      0.02s  |
|   o-> ion_atom_regin                           1296        1296      0.00s  |
|   o-> ion_atom_solve_linear                      18          18      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- basis_initialise                            2                         |
|  /                                                                          |
| O-> basis_assign_grid_coordinates                 2           2      0.07s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_prepare_and_diagonalise          24428                         |
|  /                                                                          |
| O-> phonon_constrain_dmat                     24428       24428      0.07s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_descreen_psp                       3                         |
|  /                                                                          |
| O-> ion_atom_ps_solve                             3           3      0.06s  |
|    /                                                                        |
|   o-> ion_atom_sod_solve                        617         617      0.03s  |
|   o-> ion_atom_radin                           1322        1322      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- cell_supercell                          12215                         |
|  /                                                                          |
| O-> cell_set_current_kpoints_wrapped          12215       12215      0.06s  |
+-----------------------------------------------------------------------------+
|   o-- ion_initialise                              3                         |
|  /                                                                          |
| O-> ion_set_psp                                   3           3      0.06s  |
+-----------------------------------------------------------------------------+
|   o-- ion_beta_recip_set                       2640                         |
|  /                                                                          |
| O-> basis_multiply_recip_reduced               2640        2640      0.06s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                            1                         |
|  /o-- phonon_real_to_fine                     12215                         |
| |/                                                                          |
| O-> cell_rationalise_coordinates              12216       12216      0.05s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_set_ae_H                         205                         |
|  /o-- ion_atom_descreen_psp                       3                         |
| |/                                                                          |
| O-> ion_atom_ae_xc                              208         208      0.04s  |
|    /                                                                        |
|   o-> ion_atom_real_derivative                  208         208      0.01s  |
|   o-> ion_atom_radin                            208         208      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_generate_cell_constraints          12217                         |
|  /o-- wave_initialise_wv                       1060                         |
| |/                                                                          |
| O-> algor_uniform_random_array                13277       13277      0.04s  |
|    /                                                                        |
|   o-> algor_set_random_seed                       1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_beta_recip_set                        220                         |
|  /                                                                          |
| O-> ion_cc_structure_factor                     220         220      0.04s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          1                         |
|  /                                                                          |
| O-> density_read                                  1           1      0.04s  |
|    /                                                                        |
|   o-> density_real_to_complex                     1           1      0.01s  |
|   o-> density_read_serial                         1           1      0.03s  |
|   o-> density_complex_to_real                     1           1      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_supercell                          12215                         |
| |/                                                                          |
| O-> cell_recip_lattice                        12216       12216      0.04s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_real_to_fine                     12215                         |
|  /                                                                          |
| O-> check_image_list                          12215       12215      0.04s  |
|    /                                                                        |
|   o-> cell_frac_to_cart_vector_wrapped           88          88      0.00s  |
|   o-> cell_find_reduced_cell                      1           1      0.00s  |
|   o-> cell_detect_same_cell                       1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_pseudo_scf                       108                         |
|  /                                                                          |
| O-> ion_atom_set_pseudo_H                       108         108      0.03s  |
|    /                                                                        |
|   o-> ion_atom_calc_pseudo_rho                  108         108      0.00s  |
|   o-> ion_atom_pseudo_hartree                   108         108      0.00s  |
|   o-> ion_atom_pseudo_xc                        108         108      0.00s  |
|   o-> ion_atom_regin                           4113        4113      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           2                         |
|  /o-- cell_supercell                          12215                         |
| |/                                                                          |
| O-> cell_calculate_volume                     12217       12217      0.03s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_core_pseudise                      3                         |
|  /o-- ion_atom_qc_pseudise_fn                    18                         |
|  /o-- ion_atom_Q_pseudise                        13                         |
| |/                                                                          |
| O-> ion_atom_set_qn                              34          34      0.03s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_fermi_contact                     44                         |
|  /o-- ion_atom_init_pseudo_atom               12410                         |
| |/                                                                          |
| O-> ion_atom_interpolate                      12454       12454      0.03s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_init_pseudo_atom               12410                         |
|  /                                                                          |
| O-> ion_atom_locate                           12410       12410      0.03s  |
+-----------------------------------------------------------------------------+
|   o-- wave_calc_Soverlap_wv_ks                   10                         |
|  /                                                                          |
| O-> wave_q_dot_all_self_c                        10          10      0.03s  |
|    /                                                                        |
|   o-> local_q_dot_all_self_c                     10          10      0.03s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_ps_solve                         617                         |
|  /                                                                          |
| O-> ion_atom_sod_solve                          617         617      0.03s  |
+-----------------------------------------------------------------------------+
|   o-- wave_q_dot_all_self_c                      10                         |
|  /                                                                          |
| O-> local_q_dot_all_self_c                       10          10      0.03s  |
+-----------------------------------------------------------------------------+
|   o-- density_read                                1                         |
|  /                                                                          |
| O-> density_read_serial                           1           1      0.03s  |
|    /                                                                        |
|   o-> comms_copy_kp_complex                       1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_koelling_harmon_solve           4948                         |
|  /                                                                          |
| O-> ion_atom_number_of_nodes                   4948        4948      0.02s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                            1                         |
|  /                                                                          |
| O-> phonon_print_force_const_matrix               1           1      0.02s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_set_ae_H                         205                         |
|  /o-- ion_atom_descreen_psp                       3                         |
| |/                                                                          |
| O-> ion_atom_ae_hartree                         208         208      0.02s  |
|    /                                                                        |
|   o-> ion_atom_radin                            624         624      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /                                                                          |
| O-> parameters_read                               1           1      0.02s  |
|    /                                                                        |
|   o-> parameters_keywords_setup                   1           1      0.00s  |
|   o-> comms_gcopy_integer                         3           3      0.00s  |
|   o-> parameters_read_xc_block                    1           1      0.00s  |
|   o-> parameters_validate                         1           1      0.00s  |
|   o-> parameters_bcast                            1           1      0.00s  |
|   o-> algor_set_random_seed                       1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- density_read                                1                         |
|  /o-- density_write                               1                         |
| |/                                                                          |
| O-> density_real_to_complex                       2           2      0.02s  |
|    /                                                                        |
|   o-> density_allocate                            2           2      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_pseudo_scf                         3                         |
|  /                                                                          |
| O-> ion_atom_init_pseudo_basis                    3           3      0.02s  |
|    /                                                                        |
|   o-> ion_atom_find_root                       1206        1206      0.00s  |
|   o-> ion_atom_regin                            366         366      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          1                         |
|  /                                                                          |
| O-> parameters_reread                             1           1      0.02s  |
|    /                                                                        |
|   o-> parameters_reread_integer                   4           4      0.00s  |
|   o-> parameters_reread_character                 8           8      0.00s  |
|   o-> parameters_reread_logical                   2           2      0.00s  |
|   o-> parameters_reread_physical                  4           4      0.00s  |
|   o-> parameters_reread_real                      1           1      0.00s  |
|   o-> parameters_validate                         1           1      0.00s  |
|   o-> comms_gcopy_integer                         3           3      0.00s  |
|   o-> comms_gcopy_logical                         1           1      0.00s  |
|   o-> parameters_bcast                            1           1      0.00s  |
|   o-> algor_set_random_seed                       1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_qc_pseudise_fn                  7218                         |
|  /                                                                          |
| O-> ion_atom_expjl                             7218        7218      0.02s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_core_pseudise                      3                         |
|  /o-- ion_atom_qc_pseudise_fn                    18                         |
|  /o-- ion_atom_Q_pseudise                        95                         |
| |/                                                                          |
| O-> ion_atom_solve_linear                       116         116      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_read_phonon_data                       2                         |
|  /o-- cell_read_supercell_data                    2                         |
| |/                                                                          |
| O-> cell_kpoints_mp_wrapped                       5           5      0.01s  |
|    /                                                                        |
|   o-> bib_add                                     5           5      0.00s  |
|   o-> cell_generate_MP_set_wrapped                5           5      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_ae_xc                            208                         |
|  /o-- ion_atom_real_derivative_kind1            168                         |
| |/                                                                          |
| O-> ion_atom_real_derivative                    376         376      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          1                         |
|  /o-- density_real_to_complex                     2                         |
|  /o-- density_complex_to_real                     1                         |
|  /o-- nlxc_initialise                             1                         |
|  /o-- density_write                               1                         |
| |/                                                                          |
| O-> density_allocate                              6           6      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_construct_psp_paw                168                         |
|  /                                                                          |
| O-> ion_atom_real_derivative_kind1              168         168      0.01s  |
|    /                                                                        |
|   o-> ion_atom_real_derivative                  168         168      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_calc_irreps                      5904                         |
|  /                                                                          |
| O-> secondd_determinant                        5904        5904      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /                                                                          |
| O-> parameters_output                             1           1      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- cell_kpoints_mp_wrapped                     5                         |
|  /                                                                          |
| O-> cell_generate_MP_set_wrapped                  5           5      0.01s  |
|    /                                                                        |
|   o-> bib_add                                     5           5      0.00s  |
|   o-> cell_reduce_kpoints_internal                5           5      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- cell_generate_MP_set_wrapped                5                         |
|  /o-- cell_unfold_kpoints_arg_trace               1                         |
| |/                                                                          |
| O-> cell_reduce_kpoints_internal                  6           6      0.01s  |
|    /                                                                        |
|   o-> cell_sort_kpoints_with_recip              247         247      0.01s  |
|   o-> algor_invert_real                           2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- basis_initialise                            2                         |
|  /                                                                          |
| O-> basis_assign_pw_gvectors                      2           2      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          1                         |
|  /                                                                          |
| O-> model_check_dependencies                      1           1      0.01s  |
|    /                                                                        |
|   o-> model_symmetry_unchanged                    1           1      0.01s  |
|   o-> model_ionic_constr_unchanged                1           1      0.00s  |
|   o-> model_cell_constr_unchanged                 1           1      0.00s  |
|   o-> model_pressure_unchanged                    1           1      0.00s  |
|   o-> model_efield_unchanged                      1           1      0.00s  |
|   o-> model_hubbard_u_unchanged                   1           1      0.00s  |
|   o-> model_kpoints_unchanged_2                   2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- basis_initialise                            1                         |
|  /                                                                          |
| O-> basis_assign_plane_wave_indexes               1           1      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- model_check_dependencies                    1                         |
|  /                                                                          |
| O-> model_symmetry_unchanged                      1           1      0.01s  |
|    /                                                                        |
|   o-> model_compare_matrices                   2304        2304      0.00s  |
|   o-> model_compare_vectors                      48          48      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_reduce_kpoints_internal              247                         |
|  /                                                                          |
| O-> cell_sort_kpoints_with_recip                247         247      0.01s  |
|    /                                                                        |
|   o-> cell_find_reduced_cell                    247         247      0.00s  |
|   o-> algor_invert_real                         494         494      0.00s  |
|   o-> algor_sort                                247         247      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_construct_psp_paw                 84                         |
|  /                                                                          |
| O-> ion_atom_apply_Tl                            84          84      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- basis_initialise                            1                         |
|  /                                                                          |
| O-> basis_count_plane_waves                       1           1      0.01s  |
|    /                                                                        |
|   o-> comms_reduce_gv_integer                     3           3      0.00s  |
|   o-> comms_reduce_kp_integer                     3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /                                                                          |
| O-> cell_output_wrapped                           1           1      0.01s  |
|    /                                                                        |
|   o-> cell_cart_lattice_to_abc                    1           1      0.00s  |
|   o-> cell_check_group                            1           1      0.00s  |
|   o-> cell_factor_group_symmetry_wrapped          1           1      0.00s  |
|   o-> cell_symmetry_symbol_wrapped_wrapped       48          48      0.00s  |
|   o-> cell_format_atom_label                     66          66      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_reread_wrapped                         1                         |
| |/                                                                          |
| O-> cell_read_supercell_data                      2           2      0.01s  |
|    /                                                                        |
|   o-> cell_kpoints_mp_wrapped                     2           2      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- cell_kpoints_mp_wrapped                     5                         |
|  /o-- cell_generate_MP_set_wrapped                5                         |
|  /o-- castep                                      4                         |
|  /o-- phonon_calculate                            2                         |
| |/                                                                          |
| O-> bib_add                                      16          16      0.01s  |
|    /                                                                        |
|   o-> bib_setup                                   1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_write_all                             2                         |
|  /                                                                          |
| O-> cell_dump                                     2           2      0.01s  |
|    /                                                                        |
|   o-> cell_dump_cell                              2           2      0.00s  |
|   o-> cell_dump_global                            2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /                                                                          |
| O-> bib_output                                    1           1      0.01s  |
|    /                                                                        |
|   o-> comms_reduce_farm_logical                   1           1      0.00s  |
|   o-> comms_reduce_bnd_logical                    1           1      0.00s  |
|   o-> comms_reduce_gv_logical                     1           1      0.00s  |
|   o-> comms_reduce_kp_logical                     1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- comms_reduce_gv_complex                    20                         |
|  /o-- comms_reduce_bnd_complex                   10                         |
| |/                                                                          |
| O-> comms_reduce_array_complex                   30          30      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- density_read                                1                         |
|  /                                                                          |
| O-> density_complex_to_real                       1           1      0.01s  |
|    /                                                                        |
|   o-> density_allocate                            1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_all_beta_multi_phi_recip               10                         |
|  /o-- wave_calc_Soverlap_wv_ks                   10                         |
| |/                                                                          |
| O-> comms_reduce_gv_complex                      20          20      0.01s  |
|    /                                                                        |
|   o-> comms_reduce_array_complex                 20          20      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- wave_orthonormalise_over_wv_ks             10                         |
|  /                                                                          |
| O-> algor_invert_complex                         10          10      0.01s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_set_pseudo_H                     108                         |
|  /                                                                          |
| O-> ion_atom_calc_pseudo_rho                    108         108      0.00s  |
|    /                                                                        |
|   o-> ion_atom_regin                            108         108      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_construct_psp_paw                 21                         |
|  /                                                                          |
| O-> ion_atom_partial_wave                        21          21      0.00s  |
|    /                                                                        |
|   o-> ion_atom_kh_partial_wave                   21          21      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_symmetry_unchanged                 2304                         |
|  /                                                                          |
| O-> model_compare_matrices                     2304        2304      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_init_pseudo_basis               1206                         |
|  /                                                                          |
| O-> ion_atom_find_root                         1206        1206      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_partial_wave                      21                         |
|  /                                                                          |
| O-> ion_atom_kh_partial_wave                     21          21      0.00s  |
|    /                                                                        |
|   o-> ion_atom_cfd_solve                         21          21      0.00s  |
|   o-> ion_atom_radin                             21          21      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_dump                                   2                         |
|  /                                                                          |
| O-> cell_dump_global                              2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_restore_wrapped                        2                         |
|  /                                                                          |
| O-> cell_restore_global_wrapped                   2           2      0.00s  |
|    /                                                                        |
|   o-> comms_gcopy_character                     178         178      0.00s  |
|   o-> comms_gcopy_integer                        76          76      0.00s  |
|   o-> comms_gcopy_real                           90          90      0.00s  |
|   o-> cell_detect_MP                              4           4      0.00s  |
|   o-> comms_gcopy_logical                        14          14      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- parameters_read                             1                         |
|  /o-- parameters_restore                          1                         |
|  /o-- parameters_reread                           1                         |
| |/                                                                          |
| O-> parameters_bcast                              3           3      0.00s  |
|    /                                                                        |
|   o-> comms_gcopy_logical                       123         123      0.00s  |
|   o-> comms_gcopy_character                     197         197      0.00s  |
|   o-> comms_gcopy_integer                       157         157      0.00s  |
|   o-> parameters_reallocate_xc                   21          21      0.00s  |
|   o-> comms_gcopy_real                          188         188      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_ps_diag                          354                         |
|  /                                                                          |
| O-> ion_atom_rectoreal                          354         354      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_init_pseudo_H                      3                         |
|  /o-- ion_atom_set_pseudo_H                     108                         |
| |/                                                                          |
| O-> ion_atom_pseudo_xc                          111         111      0.00s  |
|    /                                                                        |
|   o-> ion_atom_regin                            111         111      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /                                                                          |
| O-> cell_check_group_equiv                        1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /                                                                          |
| O-> nlxc_initialise                               1           1      0.00s  |
|    /                                                                        |
|   o-> density_allocate                            1           1      0.00s  |
|   o-> density_copy                                1           1      0.00s  |
|   o-> cell_generate_qpoints_local                 1           1      0.00s  |
|   o-> density_deallocate                          1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_write_all                             1                         |
|  /                                                                          |
| O-> parameters_dump                               1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_calc_irreps                       984                         |
|  /                                                                          |
| O-> secondd_irrep_symbol                        984         984      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_core_pseudise                      3                         |
|  /o-- ion_atom_pn_pseudise                        6                         |
|  /o-- ion_atom_qc_pseudise_fn                    18                         |
|  /o-- ion_atom_Q_pseudise                        95                         |
| |/                                                                          |
| O-> ion_atom_poly_deriv                         122         122      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_dump                                   2                         |
|  /                                                                          |
| O-> cell_dump_cell                                2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- nlxc_initialise                             1                         |
| |/                                                                          |
| O-> cell_generate_qpoints_local                   2           2      0.00s  |
|    /                                                                        |
|   o-> cell_detect_MP                              2           2      0.00s  |
|   o-> comms_gcopy_real                            4           4      0.00s  |
|   o-> comms_gcopy_integer                         4           4      0.00s  |
|   o-> comms_gcopy_logical                         2           2      0.00s  |
|   o-> cell_unfold_kpoints_arg_trace               1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- nlxc_initialise                             1                         |
|  /o-- density_write                               1                         |
| |/                                                                          |
| O-> density_copy                                  2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                          66                         |
|  /                                                                          |
| O-> cell_read_line_real                          66          66      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                            1                         |
|  /                                                                          |
| O-> secondd_finalise                              1           1      0.00s  |
|    /                                                                        |
|   o-> model_delete_stored_file                    3           3      0.00s  |
|   o-> raman_finalise                              1           1      0.00s  |
|   o-> secondd_deallocate_wvfns                    1           1      0.00s  |
|   o-> secondd_detect_symmetry_changed             1           1      0.00s  |
|   o-> secondd_detect_kpoints_changed              1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_sort_kpoints_with_recip              247                         |
|  /o-- check_image_list                            1                         |
| |/                                                                          |
| O-> cell_find_reduced_cell                      248         248      0.00s  |
|    /                                                                        |
|   o-> algor_invert_real                         248         248      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          1                         |
|  /                                                                          |
| O-> parameters_restore                            1           1      0.00s  |
|    /                                                                        |
|   o-> parameters_bcast                            1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_finalise                            3                         |
|  /                                                                          |
| O-> model_delete_stored_file                      3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- basis_initialise                            1                         |
|  /                                                                          |
| O-> basis_map_fine_recip_half_full                1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /                                                                          |
| O-> check_forces_stresses                         1           1      0.00s  |
|    /                                                                        |
|   o-> firstd_output_forces                        1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- check_forces_stresses                       1                         |
|  /                                                                          |
| O-> firstd_output_forces                          1           1      0.00s  |
|    /                                                                        |
|   o-> cell_format_atom_label                     22          22      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_sort_kpoints_with_recip              247                         |
|  /o-- ion_atom_output_psp                         3                         |
|  /o-- cell_detect_same_cell                       2                         |
| |/                                                                          |
| O-> algor_sort                                  252         252      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_generate_psp                       3                         |
|  /o-- ion_atom_descreen_psp                       3                         |
| |/                                                                          |
| O-> ion_atom_init_ae_atom                         6           6      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_init_pseudo_H                      3                         |
|  /o-- ion_atom_set_pseudo_H                     108                         |
| |/                                                                          |
| O-> ion_atom_pseudo_hartree                     111         111      0.00s  |
|    /                                                                        |
|   o-> ion_atom_regin                            111         111      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- basis_initialise                            1                         |
|  /                                                                          |
| O-> basis_map_standard_to_fine                    1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_calc_irreps                       123                         |
|  /                                                                          |
| O-> secondd_analyse_symmetries                  123         123      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /o-- cell_read_wrapped                          68                         |
|  /o-- cell_generate_qpoints_local                 4                         |
|  /o-- parameters_bcast                          188                         |
|  /o-- cell_restore_cell                          28                         |
|  /o-- cell_restore_global_wrapped                90                         |
|  /o-- cell_reread_wrapped                        66                         |
|  /o-- model_continuation                          7                         |
| |/                                                                          |
| O-> comms_gcopy_real                            452         452      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          1                         |
|  /o-- model_deallocate                            1                         |
| |/                                                                          |
| O-> model_reset                                   2           2      0.00s  |
|    /                                                                        |
|   o-> cell_deallocate                             8           8      0.00s  |
|   o-> wave_deallocate_wv                          2           2      0.00s  |
|   o-> density_deallocate                          2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                          28                         |
|  /o-- parameters_bcast                          197                         |
|  /o-- cell_restore_wrapped                        2                         |
|  /o-- cell_restore_cell                          12                         |
|  /o-- cell_restore_global_wrapped               178                         |
|  /o-- cell_reread_wrapped                        28                         |
|  /o-- model_continuation                          6                         |
|  /o-- phonon_initialise                           1                         |
| |/                                                                          |
| O-> comms_gcopy_character                       452         452      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                          123                         |
|  /                                                                          |
| O-> phonon_choose_gamma_directions              123         123      0.00s  |
|    /                                                                        |
|   o-> q_is_a_g                                  123         123      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                          63                         |
|  /o-- cell_generate_qpoints_local                 4                         |
|  /o-- parameters_read                             3                         |
|  /o-- parameters_bcast                          157                         |
|  /o-- parameters_reallocate_xc                   21                         |
|  /o-- cell_restore_cell                          40                         |
|  /o-- cell_restore_global_wrapped                76                         |
|  /o-- cell_reread_wrapped                        59                         |
|  /o-- model_continuation                          5                         |
|  /o-- parameters_reread                           3                         |
| |/                                                                          |
| O-> comms_gcopy_integer                         431         431      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /                                                                          |
| O-> model_deallocate                              1           1      0.00s  |
|    /                                                                        |
|   o-> model_reset                                 1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          4                         |
|  /o-- phonon_calculate                            2                         |
| |/                                                                          |
| O-> cell_copy                                     6           6      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_reset                                 2                         |
|  /o-- secondd_deallocate_wvfns                    4                         |
| |/                                                                          |
| O-> wave_deallocate_wv                            6           6      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_restore_wrapped                        2                         |
|  /                                                                          |
| O-> cell_restore_cell                             2           2      0.00s  |
|    /                                                                        |
|   o-> cell_deallocate                             2           2      0.00s  |
|   o-> comms_gcopy_real                           28          28      0.00s  |
|   o-> comms_gcopy_integer                        40          40      0.00s  |
|   o-> cell_allocate                               2           2      0.00s  |
|   o-> comms_gcopy_logical                         6           6      0.00s  |
|   o-> comms_gcopy_character                      12          12      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          1                         |
|  /                                                                          |
| O-> model_read_occ_eigenvalues                    1           1      0.00s  |
|    /                                                                        |
|   o-> comms_copy_kp_real                         30          30      0.00s  |
|   o-> comms_copy_bnd_logical                    100         100      0.00s  |
|   o-> comms_copy_bnd_real                        20          20      0.00s  |
|   o-> comms_copy_gv_real                          2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_pseudo_scf                         3                         |
|  /                                                                          |
| O-> ion_atom_init_pseudo_H                        3           3      0.00s  |
|    /                                                                        |
|   o-> ion_atom_regin                            117         117      0.00s  |
|   o-> ion_atom_pseudo_hartree                     3           3      0.00s  |
|   o-> ion_atom_pseudo_xc                          3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_generate_qpoints_local                 1                         |
|  /                                                                          |
| O-> cell_unfold_kpoints_arg_trace                 1           1      0.00s  |
|    /                                                                        |
|   o-> cell_reduce_kpoints_internal                1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_reread_wrapped                         1                         |
| |/                                                                          |
| O-> cell_setup_keywords                           2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- wave_initialise_wv                          1                         |
|  /                                                                          |
| O-> wave_prepare_init_wvfn                        1           1      0.00s  |
|    /                                                                        |
|   o-> comms_reduce_gv_integer                    20          20      0.00s  |
|   o-> comms_reduce_kp_logical                     1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                          22                         |
|  /                                                                          |
| O-> cell_read_line_char                          22          22      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- bib_add                                     1                         |
|  /                                                                          |
| O-> bib_setup                                     1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- parameters_reread                           8                         |
|  /                                                                          |
| O-> parameters_reread_character                   8           8      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_pseudise                           3                         |
|  /                                                                          |
| O-> ion_atom_pn_pseudise                          3           3      0.00s  |
|    /                                                                        |
|   o-> ion_atom_poly_deriv                         6           6      0.00s  |
|   o-> algor_invert_real                           3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- basis_initialise                            2                         |
|  /                                                                          |
| O-> basis_calculate_cut_off                       2           2      0.00s  |
|    /                                                                        |
|   o-> comms_reduce_gv_real                        2           2      0.00s  |
|   o-> comms_reduce_kp_real                        2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_initialise                              1                         |
|  /                                                                          |
| O-> ion_clebsch_gordan                            1           1      0.00s  |
|    /                                                                        |
|   o-> init_factorial                              1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_write                                 1                         |
|  /                                                                          |
| O-> io_delete_file                                1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_inquire_psp                        3                         |
|  /o-- ion_atom_generate_psp                       3                         |
| |/                                                                          |
| O-> ion_atom_init_ae_basis                        6           6      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_ae_scf                             3                         |
|  /                                                                          |
| O-> ion_atom_init_ae_H                            3           3      0.00s  |
|    /                                                                        |
|   o-> ion_atom_set_ae_occ                         3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_initialise                              1                         |
|  /                                                                          |
| O-> ion_allocate                                  1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_write_all                             1                         |
|  /                                                                          |
| O-> model_write_occ_eigenvalues                   1           1      0.00s  |
|    /                                                                        |
|   o-> comms_reduce_bnd_real                       2           2      0.00s  |
|   o-> comms_gather_kp_integer                     1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_init_ae_H                          3                         |
|  /o-- ion_atom_descreen_psp                       3                         |
| |/                                                                          |
| O-> ion_atom_set_ae_occ                           6           6      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /                                                                          |
| O-> ion_read                                      1           1      0.00s  |
|    /                                                                        |
|   o-> ion_atom_inquire_psp                        3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- basis_initialise                            1                         |
|  /                                                                          |
| O-> basis_set_serial_grids                        1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- wave_initialise_wv                          1                         |
|  /o-- wave_beta_phi_wv_ks                        10                         |
|  /o-- ion_beta_recip_set                         10                         |
| |/                                                                          |
| O-> ion_set_projectors                           21          21      0.00s  |
|    /                                                                        |
|   o-> comms_reduce_gv_logical                    21          21      0.00s  |
|   o-> comms_reduce_gv_integer                     1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_construct_psp_paw                 44                         |
|  /                                                                          |
| O-> ion_atom_fermi_contact                       44          44      0.00s  |
|    /                                                                        |
|   o-> ion_atom_interpolate                       44          44      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_read                                    3                         |
|  /                                                                          |
| O-> ion_atom_inquire_psp                          3           3      0.00s  |
|    /                                                                        |
|   o-> ion_atom_init_ae_basis                      3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- wave_orthonormalise_over_wv_ks             10                         |
|  /                                                                          |
| O-> comms_reduce_bnd_complex                     10          10      0.00s  |
|    /                                                                        |
|   o-> comms_reduce_array_complex                 10          10      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                          11                         |
|  /o-- cell_generate_qpoints_local                 2                         |
|  /o-- parameters_bcast                          123                         |
|  /o-- model_continuation                          4                         |
|  /o-- cell_restore_cell                           6                         |
|  /o-- cell_restore_global_wrapped                14                         |
|  /o-- cell_reread_wrapped                        10                         |
|  /o-- parameters_reread                           1                         |
|  /o-- secondd_detect_symmetry_changed             1                         |
|  /o-- secondd_detect_kpoints_changed              1                         |
| |/                                                                          |
| O-> comms_gcopy_logical                         173         173      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- parameters_reread                           4                         |
|  /                                                                          |
| O-> parameters_reread_physical                    4           4      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- parameters_read                             1                         |
|  /o-- parameters_reread                           1                         |
| |/                                                                          |
| O-> parameters_validate                           2           2      0.00s  |
|    /                                                                        |
|   o-> parameters_xc_allowed                      14          14      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- parameters_reread                           4                         |
|  /                                                                          |
| O-> parameters_reread_integer                     4           4      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                            1                         |
|  /                                                                          |
| O-> phonon_initialise                             1           1      0.00s  |
|    /                                                                        |
|   o-> comms_save_strategy                         1           1      0.00s  |
|   o-> comms_gcopy_character                       1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_check_dependencies                    1                         |
|  /                                                                          |
| O-> model_efield_unchanged                        1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- parameters_read                             1                         |
|  /                                                                          |
| O-> parameters_keywords_setup                     1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_define_psp                        69                         |
|  /                                                                          |
| O-> ion_atom_round_to_grid                       69          69      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_output_wrapped                        66                         |
|  /o-- firstd_output_forces                       22                         |
| |/                                                                          |
| O-> cell_format_atom_label                       88          88      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- parameters_read                             1                         |
|  /                                                                          |
| O-> parameters_read_xc_block                      1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_read_occ_eigenvalues                100                         |
|  /                                                                          |
| O-> comms_copy_bnd_logical                      100         100      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          1                         |
|  /                                                                          |
| O-> comms_parallel_strategy                       1           1      0.00s  |
|    /                                                                        |
|   o-> find_strategy                               1           1      0.00s  |
|   o-> assign_nodes                                1           1      0.00s  |
|   o-> reassign_nodes                              1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_define_psp                         3                         |
|  /o-- ion_atom_descreen_psp                       3                         |
| |/                                                                          |
| O-> ion_atom_derivative                           6           6      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- check_image_list                           88                         |
|  /                                                                          |
| O-> cell_frac_to_cart_vector_wrapped             88          88      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_set_projectors                         21                         |
|  /o-- bib_output                                  1                         |
| |/                                                                          |
| O-> comms_reduce_gv_logical                      22          22      0.00s  |
|    /                                                                        |
|   o-> comms_lcopy                                 1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           2                         |
|  /                                                                          |
| O-> cell_symmetry_test                            2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /                                                                          |
| O-> cell_find_related_atoms                       1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- parameters_bcast                           21                         |
|  /                                                                          |
| O-> parameters_reallocate_xc                     21          21      0.00s  |
|    /                                                                        |
|   o-> comms_gcopy_integer                        21          21      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_check_dependencies                    1                         |
|  /                                                                          |
| O-> model_pressure_unchanged                      1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_init_pseudo_atom                   3                         |
|  /                                                                          |
| O-> ion_atom_set_pseudo_occ                       3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- parameters_reread                           2                         |
|  /                                                                          |
| O-> parameters_reread_logical                     2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_check_cell_constraints                 2                         |
|  /o-- cell_output_wrapped                         1                         |
|  /o-- cell_symmetry_symbol_wrapped_wrapped       48                         |
| |/                                                                          |
| O-> cell_cart_lattice_to_abc                     51          51      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_initialise                              7                         |
|  /                                                                          |
| O-> ion_atom_deallocate_pspot                     7           7      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- wave_beta_phi_wv_ks                        10                         |
|  /                                                                          |
| O-> wave_calc_ps_q_nonzero                       10          10      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_symmetry_unchanged                   48                         |
|  /                                                                          |
| O-> model_compare_vectors                        48          48      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- parameters_reread                           1                         |
|  /                                                                          |
| O-> parameters_reread_real                        1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- basis_count_plane_waves                     3                         |
|  /o-- ion_set_projectors                          1                         |
|  /o-- wave_prepare_init_wvfn                     20                         |
| |/                                                                          |
| O-> comms_reduce_gv_integer                      24          24      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_read_occ_eigenvalues                 30                         |
|  /                                                                          |
| O-> comms_copy_kp_real                           30          30      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_check_dependencies                    1                         |
|  /                                                                          |
| O-> model_hubbard_u_unchanged                     1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- algor_uniform_random_array                  1                         |
|  /o-- parameters_read                             1                         |
|  /o-- wave_initialise_wv                          1                         |
|  /o-- parameters_reread                           1                         |
| |/                                                                          |
| O-> algor_set_random_seed                         4           4      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          1                         |
|  /                                                                          |
| O-> model_store_dependencies                      1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /                                                                          |
| O-> cell_count_symmetry_translations_wrappe       1           1      0.00s  |
|    /                                                                        |
|   o-> cell_factor_group_symmetry_wrapped          1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- wave_orthonormalise_over_wv_ks             10                         |
|  /                                                                          |
| O-> comms_copy_gv_complex                        10          10      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_reread_wrapped                         1                         |
| |/                                                                          |
| O-> cell_read_bs_data                             2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_read_occ_eigenvalues                 20                         |
|  /                                                                          |
| O-> comms_copy_bnd_real                          20          20      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                            1                         |
|  /                                                                          |
| O-> secondd_symmetry_init                         1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_reread_wrapped                         1                         |
| |/                                                                          |
| O-> cell_read_spectral_data                       2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- parameters_validate                        14                         |
|  /                                                                          |
| O-> parameters_xc_allowed                        14          14      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- basis_calculate_cut_off                     2                         |
|  /o-- phonon_calculate                            1                         |
| |/                                                                          |
| O-> comms_reduce_gv_real                          3           3      0.00s  |
|    /                                                                        |
|   o-> comms_reduce_array_real                     1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          1                         |
|  /                                                                          |
| O-> wave_allocate_wv                              1           1      0.00s  |
|    /                                                                        |
|   o-> wave_band_basis_initialise                  1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                            1                         |
|  /o-- model_write_occ_eigenvalues                 2                         |
| |/                                                                          |
| O-> comms_reduce_bnd_real                         3           3      0.00s  |
|    /                                                                        |
|   o-> comms_reduce_array_real                     3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_finalise                            1                         |
|  /                                                                          |
| O-> secondd_deallocate_wvfns                      1           1      0.00s  |
|    /                                                                        |
|   o-> wave_deallocate_wv                          4           4      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_reread_wrapped                         1                         |
| |/                                                                          |
| O-> cell_check_cell_constraints                   2           2      0.00s  |
|    /                                                                        |
|   o-> cell_cart_lattice_to_abc                    2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- comms_reduce_gv_real                        1                         |
|  /o-- comms_reduce_bnd_real                       3                         |
|  /o-- comms_reduce_kp_real                        1                         |
| |/                                                                          |
| O-> comms_reduce_array_real                       5           5      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_pseudo_scf                         3                         |
|  /                                                                          |
| O-> ion_atom_basis_pseudo_dealloc                 3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_descreen_psp                       3                         |
|  /                                                                          |
| O-> ion_atom_ae_dealloc                           3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- wave_prepare_init_wvfn                      1                         |
|  /o-- phonon_calculate                            1                         |
|  /o-- bib_output                                  1                         |
| |/                                                                          |
| O-> comms_reduce_kp_logical                       3           3      0.00s  |
|    /                                                                        |
|   o-> comms_lcopy                                 1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_finalise                            1                         |
|  /                                                                          |
| O-> secondd_detect_symmetry_changed               1           1      0.00s  |
|    /                                                                        |
|   o-> comms_gcopy_logical                         1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_generate_psp                       3                         |
|  /                                                                          |
| O-> ion_atom_basis_ae_dealloc                     3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_write_occ_eigenvalues                 1                         |
|  /                                                                          |
| O-> comms_gather_kp_integer                       1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_reset                                 2                         |
|  /o-- nlxc_initialise                             1                         |
|  /o-- density_write                               1                         |
| |/                                                                          |
| O-> density_deallocate                            4           4      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- basis_calculate_cut_off                     2                         |
|  /o-- phonon_calculate                            1                         |
| |/                                                                          |
| O-> comms_reduce_kp_real                          3           3      0.00s  |
|    /                                                                        |
|   o-> comms_reduce_array_real                     1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- bib_output                                  1                         |
|  /                                                                          |
| O-> comms_reduce_farm_logical                     1           1      0.00s  |
|    /                                                                        |
|   o-> comms_lcopy                                 1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_finalise                            1                         |
|  /                                                                          |
| O-> raman_finalise                                1           1      0.00s  |
|    /                                                                        |
|   o-> raman_empty_cache                           1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- check_image_list                            1                         |
|  /                                                                          |
| O-> cell_detect_same_cell                         1           1      0.00s  |
|    /                                                                        |
|   o-> algor_sort                                  2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- basis_initialise                            6                         |
|  /                                                                          |
| O-> basis_utils_prime_factors                     6           6      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_init_pseudo_atom                   3                         |
|  /                                                                          |
| O-> ion_atom_resolve_pseudo_cfg                   3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          3                         |
|  /                                                                          |
| O-> cell_distribute_kpoints_wrapped               3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                            1                         |
|  /o-- bib_output                                  1                         |
| |/                                                                          |
| O-> comms_reduce_bnd_logical                      2           2      0.00s  |
|    /                                                                        |
|   o-> comms_lcopy                                 1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_reread_wrapped                         1                         |
| |/                                                                          |
| O-> cell_check_keywords                           2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /                                                                          |
| O-> cell_detect_primitive                         1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- secondd_finalise                            1                         |
|  /                                                                          |
| O-> secondd_detect_kpoints_changed                1           1      0.00s  |
|    /                                                                        |
|   o-> comms_gcopy_logical                         1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- basis_count_plane_waves                     3                         |
|  /                                                                          |
| O-> comms_reduce_kp_integer                       3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- comms_reduce_farm_logical                   1                         |
|  /o-- comms_reduce_bnd_logical                    1                         |
|  /o-- comms_reduce_gv_logical                     1                         |
|  /o-- comms_reduce_kp_logical                     1                         |
| |/                                                                          |
| O-> comms_lcopy                                   4           4      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /o-- cell_reread_wrapped                         1                         |
| |/                                                                          |
| O-> cell_read_optics_data                         2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_construct_psp_paw                  3                         |
|  /                                                                          |
| O-> ion_atom_write_beta                           3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_atom_construct_psp_paw                  3                         |
|  /                                                                          |
| O-> ion_atom_write_pwave                          3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_continuation                          3                         |
|  /                                                                          |
| O-> parameters_nspins                             3           3      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /o-- phonon_calculate                            1                         |
| |/                                                                          |
| O-> phonon_require_gs_wvfn                        2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_initialise                           1                         |
|  /                                                                          |
| O-> comms_save_strategy                           1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- wave_allocate_wv                            1                         |
|  /                                                                          |
| O-> wave_band_basis_initialise                    1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- density_read_serial                         1                         |
|  /                                                                          |
| O-> comms_copy_kp_complex                         1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- comms_parallel_strategy                     1                         |
|  /                                                                          |
| O-> reassign_nodes                                1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_write                                 1                         |
|  /                                                                          |
| O-> comms_barrier_farm                            1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_check_dependencies                    2                         |
|  /                                                                          |
| O-> model_kpoints_unchanged_2                     2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- comms_parallel_strategy                     1                         |
|  /                                                                          |
| O-> find_strategy                                 1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_check_dependencies                    1                         |
|  /                                                                          |
| O-> model_ionic_constr_unchanged                  1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /                                                                          |
| O-> memory_system_initialise                      1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_read_occ_eigenvalues                  2                         |
|  /                                                                          |
| O-> comms_copy_gv_real                            2           2      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- wave_initialise_wv                          1                         |
|  /                                                                          |
| O-> wave_spin_type_wv                             1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- comms_parallel_strategy                     1                         |
|  /                                                                          |
| O-> assign_nodes                                  1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- raman_finalise                              1                         |
|  /                                                                          |
| O-> raman_empty_cache                             1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_write                                 1                         |
|  /                                                                          |
| O-> comms_barrier                                 1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- castep                                      1                         |
|  /                                                                          |
| O-> tddft_set_tddft_on                            1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- cell_read_wrapped                           1                         |
|  /                                                                          |
| O-> cell_check_symmetry_ops_spin                  1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- ion_clebsch_gordan                          1                         |
|  /                                                                          |
| O-> init_factorial                                1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- model_check_dependencies                    1                         |
|  /                                                                          |
| O-> model_cell_constr_unchanged                   1           1      0.00s  |
+-----------------------------------------------------------------------------+
|   o-- phonon_calculate                            1                         |
|  /                                                                          |
| O-> secondd_compare_kpoints                       1           1      0.00s  |
+-----------------------------------------------------------------------------+
Class of operation                  Time spent
COMMS                                   0.01s
COMMS_GV                                0.01s
COMMS_KP                                0.00s
COMMS_BND                               0.00s
COMMS_FARM                              0.00s
     305 different subroutines and functions were traced
Hash collisions:          8
  
