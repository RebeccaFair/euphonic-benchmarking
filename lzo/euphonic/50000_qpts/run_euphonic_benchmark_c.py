
def main():
    import sys
    if len(sys.argv) > 1:
        n_threads = int(sys.argv[1])
    else:
        n_threads = 1

    import ctypes
    mkl = ctypes.cdll.LoadLibrary('libmkl_rt.so')
    mkl.MKL_Set_Num_Threads(1)

    from euphonic.data.interpolation import InterpolationData
    import numpy as np
    seedname = 'La2Zr2O7'

#    qpts = np.tile(np.loadtxt('10meV_qpts.txt'), (5,1))
    qpts = np.load('../../50000_qpts.npy')
#    qpts = np.load('../../500000_qpts.npy')
    idata = InterpolationData(seedname)
    N = 1
    print(('Calculating phonons for {:s}\nN qpts: {:d}\nN repeats: {:d}\nN '
           'N threads: {:d}').format(seedname, len(qpts), N, n_threads))
    for i in range(N):
        idata.calculate_fine_phonons(qpts, use_c=True, nthreads=n_threads)
    print('Freqs for qpt 5000:')
    print(idata.freqs[5000])

if __name__ == '__main__':
    main()
