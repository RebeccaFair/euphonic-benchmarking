import copy
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as signal
from simphony.data.interpolation import InterpolationData
from simphony.data.phonon import PhononData
from simphony.calculate.scattering import structure_factor, sqw_map
from simphony.plot.dispersion import plot_dispersion, plot_sqw_map

def get_data(seedname, cut_dir, T):
    idata_dir = '../castep'
    phonon_dir = '../castep/' + cut_dir
    #csv_dir = cut_dir + '/output'
    csv_dir = cut_dir + '/output/negative_ebins'

    pdata = PhononData(seedname, phonon_dir)
    idata = InterpolationData(seedname, idata_dir)
    idata.calculate_fine_phonons(pdata.qpts, asr='realspace')

    csv_name = 'La2Zr2O7_2Dmesh_scqw_' + str(T) + 'K'
    data = np.genfromtxt(csv_dir + '/' + csv_name + '.csv', delimiter=',', invalid_raise=False)

    nbins = len(data)/pdata.n_qpts
    ebins = data[:nbins, 1]
    qpts = data[::nbins, 0]
    sqw = np.reshape(data[:, 2], (len(qpts), len(ebins)))

    return pdata, idata, ebins, qpts, sqw

def get_scale_factor(in1, in2, sigma=0.5):
    # For more accuracy, ignore low energy bins, and ignore outliers when
    # calculating scale
    in2_cut = in2[:, 5:]
    in1_cut = in1[:, 5:]
    ratio = in1_cut[in2_cut > 1e-9]/in2_cut[in2_cut > 1e-9]
    sigma = 0.5
    scale = np.mean(ratio[abs(ratio - np.mean(ratio)) < sigma*np.std(ratio)])
    return scale

# Parameters
curr_dir = os.getcwd()
if len(sys.argv) > 1:
    cut_dir = sys.argv[1]
else:
    cut_dir = '0-1-1_scan/'
seedname = 'La2Zr2O7'
sl = {'La': 8.24, 'Zr': 7.16, 'O': 5.803}
T=100

pdata, idata, ebins, qpts, sqw = get_data(seedname, cut_dir, T=T)

# Does Oclimax truncate frequencies/eigenvectors?
# Number of decimal places to truncate
#t = 10.**2
#idata.freqs = np.floor(idata.freqs*t)/t
#pdata.freqs = np.floor(pdata.freqs*t)/t
#na = np.newaxis
#idata.eigenvecs = np.around(idata.eigenvecs*np.sqrt(pdata.weights[:,na,na,na]), decimals=6)/np.sqrt(pdata.weights[:,na,na,na])
#pdata.eigenvecs = np.around(pdata.eigenvecs*np.sqrt(pdata.weights[:,na,na,na]), decimals=6)/np.sqrt(pdata.weights[:,na,na,na])
#t = 10.**6
#idata.eigenvecs = (np.floor(np.real(idata.eigenvecs)*np.sqrt(pdata.weights[:,na,na,na])*t)/(np.sqrt(pdata.weights[:,na,na,na])*t) +
#                  1j*np.floor(np.imag(idata.eigenvecs)*np.sqrt(pdata.weights[:,na,na,na])*t)/(np.sqrt(pdata.weights[:,na,na,na])*t))
#pdata.eigenvecs = (np.floor(np.real(pdata.eigenvecs)*np.sqrt(pdata.weights[:,na,na,na])*t)/(np.sqrt(pdata.weights[:,na,na,na])*t) +
#                  1j*np.floor(np.imag(pdata.eigenvecs)*np.sqrt(pdata.weights[:,na,na,na])*t)/(np.sqrt(pdata.weights[:,na,na,na])*t))

# Note: ebins read from Oclimax .csv might actually be energy bin minima
# so calculate ebin edges accordingly
#ebin_width = np.mean(np.diff(ebins))
#ebin_edges = np.zeros(len(ebins) + 1)
#ebin_edges[:-1] = ebins
#ebin_edges[-1] = ebins[-1] + ebin_width
# Ebin edges read from file not accurate?
#min_e = 0.000
min_e = -100
max_e = 98.40
de = 0.900 
ebin_edges = np.arange(min_e, max_e + de, de)

# Calculate and apply scale factor
dw_grid=[6, 6, 6]
dw_seedname = 'La2Zr2O7-grid'
#sf_idata = structure_factor(idata, sl, T=T, dw_grid=dw_grid)
sf_idata = structure_factor(idata, sl, T=T, dw_seedname=dw_seedname, calc_bose=True)
sf_pdata = structure_factor(pdata, sl, T=T, dw_seedname=dw_seedname, calc_bose=True)
#sqw_map(idata, ebin_edges, sl, T=T, dw_grid=dw_grid, dw_iso=True)
sqw_map(idata, ebin_edges, sl, T=T, dw_seedname=dw_seedname, calc_bose=True)
sqw_map(pdata, ebin_edges, sl, T=T, dw_seedname=dw_seedname, calc_bose=True)
# Set all E=0 bins to 0 to match Oclimax
idata.sqw_map[:, np.argmin(np.absolute(ebins))] = 0
pdata.sqw_map[:, np.argmin(np.absolute(ebins))] = 0

# Set SimPhony value to compare to (pdata or idata) and get scale factor
tol = 1e-10
#tol = 1e-8
compare = pdata.sqw_map
sqw = sqw*get_scale_factor(compare, sqw)
divide = np.divide(compare, sqw, out=1 + np.copy(compare), where=sqw>tol)

#Calculate percentage difference
i = np.where(compare > tol)

# With low frequency bins
simphony_diff = np.abs(idata.sqw_map[i] - compare[i])/compare[i]*100
oclimax_diff = np.abs(sqw[i] - compare[i])/compare[i]*100

# Find gamma equivalent points
g = (np.sum(np.mod(pdata.qpts, 1), axis=1) == 0)
g = np.tile(g, (pdata.sqw_map.shape[1], 1)).transpose()
k = np.where(compare[~g] > tol)
# Without gamma points
simphony_diff_no_g = np.abs(idata.sqw_map[~g][k] - compare[~g][k])/compare[~g][k]*100
oclimax_diff_no_g = np.abs(sqw[~g][k] - compare[~g][k])/compare[~g][k]*100

strlen = '25'
print('% Difference from .phonon:')
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('SimPhony interpolation', np.mean(simphony_diff), np.amin(simphony_diff), np.amax(simphony_diff)))
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('Oclimax', np.mean(oclimax_diff), np.amin(oclimax_diff), np.amax(oclimax_diff)))
print('Without gamma points:')
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('SimPhony interpolation', np.mean(simphony_diff_no_g), np.amin(simphony_diff_no_g), np.amax(simphony_diff_no_g)))
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('Oclimax', np.mean(oclimax_diff_no_g), np.amin(oclimax_diff_no_g), np.amax(oclimax_diff_no_g)))

# Without DW factor
idata_no_dw = copy.deepcopy(idata)
pdata_no_dw = copy.deepcopy(pdata)
sqw_map(idata_no_dw, ebin_edges, sl, T=T, calc_bose=False)
sqw_map(pdata_no_dw, ebin_edges, sl, T=T, calc_bose=False)
# Set all E=0 bins to 0 to match Oclimax
idata_no_dw.sqw_map[:, np.argmin(np.absolute(ebins))] = 0
pdata_no_dw.sqw_map[:, np.argmin(np.absolute(ebins))] = 0
tol = 1e-10
i = np.where(pdata_no_dw.sqw_map > tol)
simphony_diff_no_dw = np.abs(idata_no_dw.sqw_map[i] - pdata_no_dw.sqw_map[i])/pdata_no_dw.sqw_map[i]*100
oclimax_diff_no_dw = np.abs(sqw[i] - pdata_no_dw.sqw_map[i])/pdata_no_dw.sqw_map[i]*100
print('Without DW:')
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('SimPhony interpolation', np.mean(simphony_diff_no_dw), np.amin(simphony_diff_no_dw), np.amax(simphony_diff_no_dw)))
print(('{:' + strlen + 's} |mean - {: 1.2e} |min - {: 1.2e} |max - {: 1.2e}').format('Oclimax', np.mean(oclimax_diff_no_dw), np.amin(oclimax_diff_no_dw), np.amax(oclimax_diff_no_dw)))

pct = np.abs(pdata.sqw_map - sqw)*100/pdata.sqw_map
pct[np.absolute(pdata.sqw_map) < tol] = 0

#qpt = 360
#qpt=461
qpt=31
qpt=0
f1 = plt.figure(1)
plt.plot(ebins, sqw[qpt, :], label='oclimax')
plt.plot(ebins, pdata.sqw_map[qpt, :], label='pdata')
plt.plot(ebins, idata.sqw_map[qpt, :], label='idata')
#plt.plot(ebins, idata_no_dw.sqw_map[qpt, :], label='pdata no dw')
plt.title('Energy Bin Intensities at ' + str(idata.qpts[qpt]))
plt.xlabel('Energy (meV')
plt.legend()
f1.show()

qpt = 200
f2 = plt.figure(2)
plt.plot(ebins, sqw[qpt, :], label='oclimax')
plt.plot(ebins, pdata.sqw_map[qpt, :], label='pdata')
plt.plot(ebins, idata.sqw_map[qpt, :], label='idata')
#plt.plot(ebins, idata_no_dw.sqw_map[qpt, :], label='pdata no dw')
plt.title('Energy Bin Intensities at ' + str(idata.qpts[qpt]))
plt.xlabel('Energy (meV')
plt.legend()
f2.show()

qpt = idata.n_qpts - 10
f3 = plt.figure(3)
plt.plot(ebins, sqw[qpt, :], label='oclimax')
plt.plot(ebins, pdata.sqw_map[qpt, :], label='pdata')
plt.plot(ebins, idata.sqw_map[qpt, :], label='idata')
#plt.plot(ebins, idata_no_dw.sqw_map[qpt, :], label='pdata no dw')
plt.title('Energy Bin Intensities at ' + str(idata.qpts[qpt]))
plt.xlabel('Energy (meV')
plt.legend()
f3.show()

f4 = plt.figure(4)
plt.imshow(divide, vmax=1.1, vmin=0.9)
plt.colorbar()
plt.show()
