
def main():
    import sys
    if len(sys.argv) > 2:
        nthreads = int(sys.argv[2])
    else:
        nthreads = int(1)
    if len(sys.argv) > 1:
        nprocs = int(sys.argv[1])
    else:
        nprocs = int(1)

    import ctypes
    blas = ctypes.cdll.LoadLibrary('/home/vol03/scarf453/.local/lib/python3.6/site-packages/numpy/.libs/libopenblasp-r0-34a18dc3.3.7.so')
    #blas = ctypes.cdll.LoadLibrary('libopenblas.so')
    blas.openblas_set_num_threads(nthreads)

    import numpy as np
    from timeit import default_timer as timer
    from euphonic.data.interpolation import InterpolationData
    seedname = 'Nb-181818-s0.5-NCP19-vib-disp'

    qpts = np.load('../../../50000_qpts.npy')
    N = 5
    times = np.zeros(N)
    idata = InterpolationData(seedname, path='../../castep')

    # Time interpolation
    start = timer()
    idata.calculate_fine_phonons(qpts, asr='reciprocal', nprocs=nprocs)
    end = timer()
    print('\nINTERPOLATION INIT RUN: sname: {:s} n_qpts: {:d} n_cores: {:d} time: {:f}s'.format(
              seedname, len(qpts), nprocs, end - start))
    for i in range(N):
        start = timer()
        idata.calculate_fine_phonons(qpts, asr='reciprocal', nprocs=nprocs)
        end = timer()
        times[i] = end - start
        print('INTERPOLATION RUN {:d}: sname: {:s} n_qpts: {:d} n_cores: {:d} time: {:f}s'.format(
                  i, seedname, len(qpts), nprocs, times[i]))
    print('INTERPOLATION MEAN TIME: {:f}s'.format(np.mean(times)))

    # Time structure factor
    sl = {'La': 8.24,
          'Zr': 7.16,
          'O': 5.803,
          'C': 6.646,
          'Si': 4.1491,
          'H': -3.7390,
          'N': 9.36,
          'S': 2.847,
          'Nb': 7.054}
    sf_times = np.zeros(N)
    for i in range(N):
        start = timer()
        sf = idata.calculate_structure_factor(sl, T=100, dw_arg=[5,5,5], asr='reciprocal', nprocs=nprocs, path='../../castep')
        end = timer()
        sf_times[i] = end - start
        print('\nSF RUN {:d}: sname: {:s} n_qpts: {:d} n_cores: {:d} time: {:f}s'.format(
                  i, seedname, len(qpts), nprocs, sf_times[i]))
    print('SF MEAN TIME: {:f}s'.format(np.mean(sf_times)))

if __name__ == '__main__':
    main()
